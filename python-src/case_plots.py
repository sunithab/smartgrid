import numpy as np
import matplotlib.pyplot as plt


def plot_cases_time() :
    num_hidden_layers_x = np.asarray([2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100])

    case_9=np.asarray(
        [4.47, 9.6777777778, 18.4222222222, 36, 53.7555555556, 72, 90, 109.333333332, 322.000000002, 4450.666666668, 12658.000000002, 31818])

    case_14=np.asarray([4.7, 9.46, 18.05, 35.32, 52.8, 72, 89.4, 108, 139.8, 4744.2, 9717, 27369])

    case_30=np.asarray([4.42, 9.5444444444, 18.03, 35.3222222222, 52.7222222222, 72, 90, 108, 355.999999998, 4247.333333334, 9927, 14802])

    case_57 = np.asarray([7.13, 13.3125, 25.72, 50.7888888889, 77.4, 104.666666664, 132.666666666, 161.4, 233.333333334, 6582.666666666, 13111.8, 14745])

    print case_9.shape , case_14.shape
    plt.plot(num_hidden_layers_x, case_9, 'g-*', label="case_9")
    plt.plot(num_hidden_layers_x, case_14, 'b-v', label="case_14")
    plt.plot(num_hidden_layers_x, case_30, 'k-o', label="case_30")
    plt.plot(num_hidden_layers_x, case_57, 'r-s', label="case_57")

    plt.xlim([1, 101])
    plt.xlabel("Number of hidden layers", fontsize=15)
    plt.ylabel("Time in seconds ", fontsize=15)
    plt.legend(loc="upper left")
    #plt.title("Accuracies for various IEEE test systems", fontsize=20)
    plt.savefig('fig_all_time_nolog.pdf', format='pdf')
    plt.show()


def plot_cases_layers_acc() :
    num_hidden_layers_x = np.asarray([2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100])

    case_9=np.asarray(
        [0.6669281046, 0.5916194626, 0.7517501816, 0.8134785766, 0.3588671024, 0.3635875091, 0.7670007262, 0.4961220044, 0.4220043573, 0.731416122, 0.5614814815, 0.4663471314 ])

    case_14=np.asarray([0.5959340659, 0.5289010989, 0.3854945055, 0.4657142857, 0.5354945055, 0.4530769231, 0.6223076923, 0.5239560439, 0.551978022, 0.386043956, 0.473956044, 0.4640659341])

    case_30=np.asarray([0.7825735294, 0.786127451, 0.7410294118, 0.7603921569, 0.6852287582, 0.7554901961, 0.7746895425, 0.7767320261, 0.7722385621, 0.8065522876, 0.7833088235, 0.7591666667])

    case_57 = np.asarray([0.9616788321, 0.8836678832, 0.95, 0.5393349554, 0.8576642336, 0.9720194647, 0.8300486618, 0.8275182482, 0.7950202758, 0.6711273317, 0.6788321168, 0.7217153285])

    print case_9.shape , case_14.shape
    plt.plot(num_hidden_layers_x, case_9, 'g-*', label="case_9")
    plt.plot(num_hidden_layers_x, case_14, 'b-v', label="case_14")
    plt.plot(num_hidden_layers_x, case_30, 'k-o', label="case_30")
    plt.plot(num_hidden_layers_x, case_57, 'r-s', label="case_57")

    plt.xlim([1, 101])
    plt.ylim([0, 1])
    plt.xlabel("Number of hidden layers", fontsize=15)
    plt.ylabel("Accuracy ", fontsize=15)
    plt.legend(loc="lower right")
    #plt.title("Accuracies for various IEEE test systems", fontsize=20)
    plt.savefig('fig_all_layer_acc.pdf', format='pdf')
    plt.show()


def plot_test_1() :
    import matplotlib.pyplot as plt

    old_x=np.asarray(range(1,24))/23.
    old_y=np.asarray([0.125, 0.1, 0.15, 0.19,  0.2573913 ,0.29086957,  0.32434783,  0.35782609,
           0.39130435, 0.43478261, 0.47826087, 0.52173913, 0.56521739, 0.60869565, 0.65217391,
           0.69565217, 0.73913043, 0.7826087, 0.82608696, 0.86956522, 0.91304348, 0.95652174, 1. ])
    old_t_y=1-old_y

    #new_x=np.asarray(range(1,23))/22.
    #new_iid_y=np.asarray([0.978102, 0.927007, 0.877372, 0.861314, 0.825547,  0.779562,  0.735766, 0.682482,
    #           0.634307, 0.580292, 0.541606, 0.559854, 0.610219, 0.609489 , 0.652555, 0.697810 ,
    #           0.747445, 0.795620,  0.846715, 0.897810 , 0.948905, 0.950000])

    #new_iid_y=np.asarray([0.978102, 0.927007, 0.877372, 0.861314, 0.825547,  0.779562,  0.745766, 0.722482,
    #           0.684307, 0.650292, 0.621606, 0.643854, 0.660219, 0.689489 , 0.712555, 0.747810 ,
    #           0.797445, 0.825620,  0.866715, 0.897810 , 0.948905, 0.950000])

    old_svm=np.asarray([0.97, 0.93, 0.89, 0.87, 0.84, .82, 0.79, .77,.7, 0.69, 0.7, 0.92, 0.58, 0.61, 0.64, 0.69, .72,
                        .79, .83, .87, 0.92, .97,1])

    print old_svm.shape
    new_iid_y=np.asarray([0.9923357664,0.9748175182,0.9627737226,0.945620438,0.9244525547,0.9142335766,0.902919708,
                        0.8751824818,0.8412408759,0.8076642336,0.7919708029,0.8104956268,0.7795620438,0.7750865052,
                        0.7811447811,0.7913322632,0.7913322632,0.8250336474,0.8470588235,0.8600645856,0.8818181818,
                        0.8907563025,0.9131313131])
    print np.shape(old_x) ,np.shape(old_y), np.shape(new_iid_y)

    plt.plot(old_x, old_y, 'g-*',label="SVE" )
    plt.plot(old_x, old_svm, 'b-*',label="SVM" )
   # plt.plot(old_x, np.maximum(old_y, 1-old_y ), 'b-*', label="perceptron" )
    plt.plot(old_x, new_iid_y, 'r-*', label="deep learning")
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel("Ratio of attacked meters -> k/m")
    plt.ylabel("Accuracy")
    plt.legend(loc="lower right")
    plt.show()


def plot_test() :
    old_x = np.asarray(range(1, 24)) / 23.

    old_y=np.asarray([0.125, 0.1, 0.15, 0.19,  0.2573913 ,0.29086957,  0.32434783,  0.35782609,
           0.39130435, 0.43478261, 0.47826087, 0.52173913, 0.56521739, 0.60869565, 0.65217391,
           0.69565217, 0.73913043, 0.7826087, 0.82608696, 0.86956522, 0.91304348, 0.95652174, 1. ])
    old_t_y=1-old_y

    old_svm=np.asarray([0.97, 0.93, 0.89, 0.87, 0.84, .82, 0.79, .77,.7, 0.69, 0.7, 0.92, 0.58, 0.61, 0.64, 0.69, .72,
                        .79, .83, .87, 0.92, .97,1])

    print old_svm.shape
    case_57 = np.asarray(
        [0.8076642336, 0.9923357664, 0.8751824818, 0.8412408759, 0.7919708029, 0.8104956268, 0.7795620438,
         0.7750865052, 0.7811447811, 0.7913322632, 0.7913322632, 0.8250336474, 0.8470588235, 0.8600645856,
         0.8818181818, 0.8907563025,  0.902919708, 0.9131313131, 0.9142335766,  0.9244525547, 0.945620438, 0.9627737226,  0.9748175182])
    print np.shape(old_x) ,np.shape(old_y), np.shape(case_57)

    plt.plot(old_x, old_y, 'g-*',label="SVE" )
    plt.plot(old_x, old_svm, 'b-*',label="SVM" )
   # plt.plot(old_x, np.maximum(old_y, 1-old_y ), 'b-*', label="perceptron" )
    plt.plot(old_x, case_57, 'r-*', label="deep learning")
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel("Ratio of attacked meters ( $\kappa$ /m) ", fontsize=15)
    plt.ylabel("Accuracy", fontsize=15)
    plt.legend(loc="lower right")
    #plt.title("Accuracy of various methods for IEEE 57-bus test system", fontsize=20)
    #plt.savefig('fig_57_acc.pdf', format='pdf')
    plt.show()


def cases_plot():
    case_9=np.asarray(
        [0.7608695652, 0.8779069767, 0.8916256158, 0.9318885449, 0.9702702703, 0.9699863574, 0.9701086957,
         0.9703504043, 0.970027248, 0.970027248, 0.9698216735, 0.9699863574, 0.9701492537, 0.9700680272,
         0.9703504043, 0.9701897019, 0.9700680272, 0.9701086957, 0.9702702703, 0.9701502703, 0.9701086957, 0.9702702703, ])

    case_14=np.asarray(
        [0.6103896104, 0.6449275362, 0.7307692308, 0.7632850242, 0.7983539095, 0.8129770992, 0.8558823529,
         0.9105839416, 0.9413875598, 0.9416666667, 0.9415274463, 0.9412470024, 0.9416666667, 0.9415274463,
         0.9414575866, 0.9415274463, 0.9417360285, 0.941943128, 0.9417360285, 0.9412470024, 0.9415971395, 0.9411057692 ])

    case_30=np.asarray(
        [0.7950819672, 0.5748502994, 0.5520833333, 0.612745098, 0.5650406504, 0.6282527881, 0.6446886447,
         0.700617284, 0.7385444744, 0.7581047382, 0.7717647059, 0.7858719647, 0.8148854962, 0.8472440945,
         0.8671232877, 0.887340302, 0.9083175803, 0.908230842, 0.9179357022, 0.9180743243, 0.9181434599, 0.9181434599 ])

    case_57 = np.asarray(
        [0.9720194647, 0.9923357664, 0.8751824818, 0.8412408759, 0.7919708029, 0.8104956268, 0.7795620438,
         0.7750865052, 0.7811447811, 0.7913322632, 0.7913322632, 0.8250336474, 0.8470588235, 0.8600645856,
         0.8818181818, 0.8907563025,  0.902919708, 0.9131313131, 0.9142335766,  0.9244525547, 0.945620438, 0.9627737226,  0.9748175182])

    case_118=np.asarray(
        [ 0.9554565702, 0.8993710692, 0.8461538462, 0.800744879, 0.7715827338, 0.6964285714, 0.6682098765,
          0.7052117264, 0.6707692308, 0.6187766714, 0.6115007013, 0.56640625, 0.6040100251, 0.5784574468,
          0.5845959596, 0.5702671312, 0.5492289442, 0.5847362514, 0.579954955, 0.5839909808, 0.5824053452, 0.5836820084 ])

    x_23 = np.asarray(range(1, 24)) / 23.
    x_22 = np.asarray(range(1, 23)) / 22.
    print case_9.shape , case_14.shape
    plt.plot(x_22, case_9, 'g-*', label="case_9")
    plt.plot(x_22, case_14, 'b-v', label="case_14")
    plt.plot(x_22, case_30, 'k-o', label="case_30")
    fig=plt.plot(x_23, case_57, 'r-s', label="case_57")
    #plt.plot(x_22, case_118, 'm-*', label="case_118")
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel("Ratio of attacked meters ( $\kappa$ /m) ", fontsize=15)
    plt.ylabel("Accuracy", fontsize=15)
    plt.legend(loc="lower right")
    #plt.title("Accuracies for various IEEE test systems", fontsize=20)
    plt.savefig('fig_all_acc.pdf', format='pdf')
    plt.show()



#plot_test()
#cases_plot()
plot_cases_layers_acc()
#plot_cases_time()
#plot_test_1() 
