function [H] = generate_mete_H(caseNum) 

BIG_H = 0;
results = rundcopf(caseNum);

x = results.bus(:, 9);
bus_id = results.bus(:, 1);

N_bus = length(x);

Pinj = results.branch(:, 14);
N_branch = length(Pinj);

if (BIG_H == 1)

    H = zeros(N_branch, N_bus);

    for i = 1:N_branch
        from_idx = find(bus_id == results.branch(i,1));
        to_idx = find(bus_id == results.branch(i,2));
        if (x(from_idx) ~= x(to_idx))
            H(i, from_idx) = Pinj(i)/(x(from_idx)-x(to_idx)); % results.branch(i,1) == from
        else
            H(i, from_idx) = 1;
        end

        H(i, to_idx) = -H(i, from_idx);                                      % results.branch(i,1) == to
    end
    



else % smaller H

    H = zeros(N_branch + N_bus, N_bus);


    for i = 1:N_branch
        from_idx = find(bus_id == results.branch(i,1));
        to_idx = find(bus_id == results.branch(i,2));
        if (x(from_idx) ~= x(to_idx))
            H(i, from_idx) = Pinj(i)/(x(from_idx)-x(to_idx)); % results.branch(i,1) == from
        else
            H(i, from_idx) = 1;
        end

        H(i, to_idx) = -H(i, from_idx);                                      % results.branch(i,1) == to
   
        H(from_idx + N_branch, from_idx) = H(from_idx + N_branch, from_idx) + H(i, from_idx);
        H(from_idx + N_branch, to_idx) = H(i, to_idx);
        
        H(to_idx + N_branch, to_idx) = H(to_idx + N_branch, to_idx) + H(i, to_idx);
        H(to_idx + N_branch, from_idx) = H(i, from_idx);

    end


end


%node_degrees = sum(H(N_branch+1:N_branch+N_bus, :) ~= 0) -1;

%stem(1:N_bus-1, node_degrees(2:end), 'r');

%z_raw = H*x;
%xhat = inv(H'*H)*H'*z_raw;
%Horg = H;

H = H(:, 2:end);
end