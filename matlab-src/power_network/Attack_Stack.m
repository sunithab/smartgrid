function [attackvector]=Attack_Stack(H)
[rows,cols]=size(H);
maxvector=H(:,1);
max=zerocounter(H(:,1));
for i=2:cols
    if(zerocounter(H(:,i))>=max)
       maxvector=H(:,i);
       max=zerocounter(H(:,i));
    end
end
stack=H;
while (~isempty(stack))
     h=stack(:,1);
     stack(:,1)=[];
     [m, n]=size(stack);
     length=n;
     for i=1:length
         t=stack(:,i);
         if(norm(h-t)<1e-6)
            continue;
         end
        [newh,d]=diffi(h',t'); 
        newh=newh'; 
         if(d<=0) 
            if(max<zerocounter(newh))
                  max=zerocounter(newh);
                  maxvector=newh;
                  stack=horzcat(stack,newh);
            end
         end
     end
end
fprintf('\nThe best attack vector obtained by stack method has %d zero elements\n',max);
attackvector=maxvector;
