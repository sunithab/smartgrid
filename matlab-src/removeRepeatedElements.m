function [ output_matrix ] = removeRepeatedElements( input_matrix )
% Removes repeated element from the matrix. It uses all but last one column
% to determine repeated rows and then removes all the rows from the matrix
% that have repeated. It keeps only the rows of the matrix that appear only
% once in the input_matrix

    [C,ia,ic] = unique(input_matrix(:,1:end-1),'rows','stable');
    elementCounts=hist(ic,numel(ia));
    output_matrix=input_matrix(ia(elementCounts==1),:);
    output_matrix=output_matrix(randperm(size(output_matrix,1)),:);
end

