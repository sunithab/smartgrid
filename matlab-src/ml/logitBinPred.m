function [y, p] = logitBinPred(model, X)
% Prediction of binary logistic regression model
% Input:
%   model: trained model structure
%   X: n x d testing data
% Output:
%   y: n x 1 predict label (0/1)
%   p: n x 1 predict probability [0,1]
% Written by Mo Chen (sth4nth@gmail.com).
X = X';
X = [X;ones(1,size(X,2))];
w = model.w;
p = exp(-log1pexp(w'*X)); 
y = round(p);
y=y';
p=p';

