function [attackvector]=Attack_Bru(H)
[m,n]=size(H);
I=eye(m,m);
flag=0;
P=H*inv(H'*H)*H'-I;
for r=1:m-n+1 
   if(flag==1)
      break;
   end
   c=zeros(1,r);
   for j=1:r
      c(j)=j;
      %fprintf('%d ',c(j));  
   end
       %fprintf('\n');
       length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       [a,b]=size(A);
       if(rank(A)<min(a,b))
             break;
       end  
   i=r;
   while(i>0)
       if (c(i)<m-r+i)
           c(i)=c(i)+1;
           for j=i+1:r
               c(j)=c(j-1)+1;
           end
       for j=1:r
           % fprintf('%d ',c(j));
       end
           %fprintf('\n');
       length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       [a,b]=size(A);
       if(rank(A)<min(a,b))
             flag=1;
             break;
       end
        i=r;  
      else
        i=i-1;
       end
  end
end

attackvector=zeros(m,1);
length_c=length(c);
I=eye(size(pinv(A)*A));
z=10*ones(max(size(I)),1);
x=(I-pinv(A)*A)*z;
for i=1:length_c
      attackvector(c(i))=x(i);
end
fprintf('\nThe best attack vector obtained by brute force method has %d zero elements\n',m-length_c);
