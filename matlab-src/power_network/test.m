 clc
clear
N=8;

%% Contruct db4 matrix

a1=(1+sqrt(3))/(4*sqrt(2));a2=(3+sqrt(3))/(4*sqrt(2));a3=(3-sqrt(3))/(4*sqrt(2));a4=(1-sqrt(3))/(4*sqrt(2));
V1=[a1, a2, a3, a4, zeros(1,N-4)];
V=V1;
for i=1:N/2-1
    V=[V;circshift(V1',2*i)'];
end

b1=(1-sqrt(3))/(4*sqrt(2));b2=(sqrt(3)-3)/(4*sqrt(2));b3=(3+sqrt(3))/(4*sqrt(2));b4=(-1-sqrt(3))/(4*sqrt(2));
W1=[b1,b2,b3,b4,zeros(1,N-4)];
W=W1;
for i=1:N/2-1
    W=[W;circshift(W1',2*i)'];
end

F=[V;W];

%% Start Simulation
% newF=[1 0 3 0;
%             5 0 7 8;
%             9 0 0 6;
%             0 2 0 5;
%             11 0 0 0;
%              0 0 1 2;
%              1 9 8 9
%              0 0 6 1
%              0 1 1 0
%              0 1 1 1
%              1 12  0 16];

% 
% newF=[1 2 3 4;
%             5 6 7 8;
%             9 10 11 12;
%             13 14 16 16;
%             19 18 19 21;];

% newF=[F; 0 0 0 0 0 0 0 1; 0 0 1 0 0 0 0 0];

% casenumber=14;
% [Z,RelationMatrix]=GetMeasurements(casenumber);
% [IEEE9_e, IEEE14_e, IEEE30_e, IEEE118_e,IEEE9_f, IEEE14_f, IEEE30_f, IEEE118_f] = Basic_IEEE_Data;
% [IEEE9_T, IEEE14_T, IEEE30_T, IEEE118_T] = Basic_IEEE_BranchData;
%   if(casenumber==9)
%      e=IEEE9_e;
%      f=IEEE9_f;
%      BranchMatrix=IEEE9_T;
%   end
%   if(casenumber==14)
%      e=IEEE14_e;
%      f=IEEE14_f;
%      BranchMatrix=IEEE14_T;
%   end
%   if(casenumber==30)
%      e=IEEE30_e;
%      f=IEEE30_f;
%      BranchMatrix=IEEE30_T;
%   end
%   if(casenumber==118)
%      e=IEEE118_e;
%      f=IEEE118_f;
%      BranchMatrix=IEEE118_T;
%   end
%  [newF]=GetJocobianMatrix(e,f,BranchMatrix,casenumber);
%  newF;

load('IEEE118DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
newF=newF(:,2:cols);
load('XData118.mat');
 x=savedata';
 x(1)=[];

 
 [rows,cols]=size(newF);
 x=randperm(cols);
 x=x';
 y=newF*x;
oldy=y;
fprintf('\nThe matrix H is a %d-by-%d matrix\n',rows,cols);

% find the column with the smallest number of nonzero elements
maxvector=newF(:,1);
max=zerocounter(newF(:,1));
for i=2:cols
    if(zerocounter(newF(:,i))>=max)
       maxvector=newF(:,i);
       max=zerocounter(newF(:,i));
    end
end
maxvector;
fprintf('\nThe smallest column has %d zero elements \n',max);
zerocounter(newF(:,1))
k=7; % k is the number of non-zero elements

for k=1:10
%find the attack vector with k non-zero elements using the stack method
%tic
 attackvector=Attack_Stack_k(newF,k)
%time1noc(k-3)=toc;
y=oldy+attackvector*10;
x_bad=inv(newF'*newF)*newF'*y;
consistency=norm(y-newF*inv(newF'*newF)*newF'*y);
end

%find the attack vector with the minimum number of non-zero elements using the stack method
% tic
%  attackvector=Attack_Stack(newF)
% toc
% y=oldy+attackvector*1000;
% x_bad=inv(newF'*newF)*newF'*y;
% consistency=norm(y-newF*inv(newF'*newF)*newF'*y)

% % find the attack vector with k non-zero elements using the brute force method
 tic 
    attackvector=Attack_Bru_k(newF,k)
 time2noc(k-3)=toc;
 y=oldy+attackvector;
 x_bad=inv(newF'*newF)*newF'*y;
 consistency=norm(y-newF*inv(newF'*newF)*newF'*y);

time1noc
time2noc
%  
% %find the attack vector with the smallest number of non-zero elements
% %using the brute force method
% tic 
%     attackvector=Attack_Bru(newF)
%  toc
% y=oldy+attackvector*1000;
% x_bad=inv(newF'*newF)*newF'*y;
% consistency=norm(y-newF*inv(newF'*newF)*newF'*y)
% 
% %find the attack vector with the specific c and k non-zero elements
% %using the brute force method
temp=randperm(cols);
for i=1:6 
c=zeros(cols,1);
for j=1:i
  c(temp(j))=100;
end
 tic 
    [attackvector,t]=Attack_Bru_c(newF,c)
 toc
 y=oldy+attackvector;
 x_bad=inv(newF'*newF)*newF'*y;
 c=x_bad-x
 consistency=norm(y-newF*inv(newF'*newF)*newF'*y)

%find the attack vector with the specific c and k non-zero elements
%using the MP method
c=zeros(cols,1);
for j=1:i
  c(temp(j))=100;
end
 tic 
   %change [x,tau]=Matching_Pursuit_k(P,y,k) in Attack_mp_kc to
   % [x,tau]=Matching_Pursuit(P,y) to find the near optimal solution
   [attackvector,t]=Attack_mp_kc(newF,k,c) %Attack_Stack_kc(newF,k,c)
 toc
 y=oldy+attackvector;
 x_bad=inv(newF'*newF)*newF'*y;
 x_bad
 x
 c=x_bad-x
 consistency=norm(y-newF*inv(newF'*newF)*newF'*y)
end


