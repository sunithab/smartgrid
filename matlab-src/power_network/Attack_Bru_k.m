function [attackvector]=Attack_Bru_k(H,k)
[m,n]=size(H);
I=eye(m,m);
flag=0;
P=H*inv(H'*H)*H'-I;
r=k;
c=zeros(1,r);
   for j=1:r
      c(j)=j;
 %     fprintf('%d ',c(j));  
   end
   %    fprintf('\n');
       length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       [a,b]=size(A);
       if(rank(A,1e-5)<min(a,b))%  if(rank(A)<min(a,b))
         I=eye(size(pinv(A)*A));
         if(norm(I-pinv(A,1e-5)*A)>1e-4)
             attackvector=zeros(m,1);
             length_c=length(c);
             I=eye(size(pinv(A)*A));
             z=10*ones(max(size(I)),1);
             x=(I-pinv(A,1e-5)*A)*z; %x=(I-pinv(A)*A)*z;
            for i=1:length_c
                  attackvector(c(i))=x(i);
            end
             flag=1;
             fprintf('\nBrute Force Bingo! The %d  attack vector is:\n',m-k);
            return;
         end
       end  
   i=r;
   
   while(i>0)
       if (c(i)<m-r+i)
           c(i)=c(i)+1;
           for j=i+1:r
               c(j)=c(j-1)+1;
           end
       for j=1:r
    %        fprintf('%d ',c(j));
       end
      
       %     fprintf('\n');
%       c(1)=2;
%       c(2)=8;
%       c(3)=16;
%       c(4)=25;
%       if(c(1)==2 && c(2)==8 && c(3)==16 && c(4)==25)
%           printf('bingo!\n');
%       end
       length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       [a,b]=size(A);
       if(rank(A,1e-5)<min(a,b)) %  if(rank(A)<min(a,b))
           I=eye(max(size(pinv(A)*A)));
           if(norm(I-pinv(A,1e-5)*A)>1e-4)
             flag=1;
             break;
           end
       end
        i=r;  
      else
        i=i-1;
       end
   end
if(flag==1)
    attackvector=zeros(m,1);
    length_c=length(c);
    I=eye(max(size(pinv(A)*A)));
    z=10*ones(max(size(I)),1);
    %A=A/(1e+13);
    x=(I-pinv(A,1e-10)*A)*z; %x=(I-pinv(A)*A)*z;
    for i=1:length_c
          attackvector(c(i))=x(i);
    end
else
   attackvector=zeros(m,1); 
end

if(zerocounter(attackvector)==m)
 %    fprintf('\n%d attack vector does not exist !\n',m-k);
 else
    fprintf('\nBrute Force K Bingo! The %d  attack vector is:\n',m-k);
 end