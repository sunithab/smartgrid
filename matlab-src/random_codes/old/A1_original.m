%Implementation for Algorithm 1
N=14;
load('IEEE14DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
H=newF(:,2:cols);
[m,n]=size(H);
K=H*inv(H'*H)*H';


mpc = loadcase('case14');
define_constants;
original_total=sum(mpc.bus(:,PD));
L = get_data();
A=zeros(14,11);
A(2,1)=1;
A(3,2)=1;
A(4,3)=1;
A(5,4)=1;
A(6,5)=1;
A(9,6)=1;
A(10,7)=1;
A(11,8)=1;
A(12,9)=1;
A(13,10)=1;
A(14,11)=1;
[r,length]=size(L);
Z=zeros(m,length);

for i=1:length
    d=L(:,i);
    ratio=sum(d)/original_total;
    d=d/ratio;
    load=A*d;
    mpc.bus(:,PD)=load;
    mpopt=mpoption('out.all',0);
    results=rundcpf(mpc,mpopt);
    x=results.bus(:,9);
    x=x(2:14,1);
    z=H*x;
    Z(:,i)=z;
end

delta=100;
Winv=delta*eye(n);
J=randn(m,n);
p=0.98;
index=(1:length)';
error=zeros(length,1);
K_error=zeros(length,1);

total_cputime = 0;
for i=1:length
t0 = cputime;    
e=normrnd(0,0.1,[m 1]);
% make z is not linear dependent with each other
c=rand(n,1)*i;
a=H*c;
z1=Z(:,i)+a+e;
x=inv(J'*J)*J'*z1;
beta=1+1/p*x'*Winv*x;
alpha=1/p*Winv*x;
Winv=1/p*Winv-1/beta*alpha*alpha';
for j=1:m
    temp=J(j,:)';
    temp=temp+(z1(j,1)-x'*temp)*Winv*x;
    J(j,:)=temp';
end

total_cputime = total_cputime+ cputime-t0;
 [Uq,Us,Ur] = svd(J,0);
  error(i,1)=norm((eye(m)-Uq*Uq')*H,'fro')/norm(H,'fro');
  K_prime=J*inv(J'*J)*J';
  K_error(i,1)=norm(K_prime-K,'fro');
end
display(total_cputime/length);
%M=[index,error,K_error];
%semilogy(index,error);
%dlmwrite('../data/R1/data.txt',M, ' ');

