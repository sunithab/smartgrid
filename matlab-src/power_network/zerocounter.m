function [zero_num]=zerocounter(x)
   zero_num=0;
   for i=1:length(x)
       if(abs(x(i))<1e-6)
           zero_num=zero_num+1;
       end
   end