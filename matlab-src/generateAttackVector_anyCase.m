function []= generateAttackVector_anyCase(caseNum, numOfSamples, numOfAttackedMeters, isTestData) 
%{
caseNum : Determines which type of IEEE test system needs to be loaded.
numOfSamples: Number of samples needed; Each sample consists of all the
meter readings.
numOfAttackedMeters: Num of meters to be attacked.
%}
    if nargin < 4
        error('Less inputs provided; Function requires 4 inputs: \n generateAttackVector_anyCase(caseNum, numOfSamples, numOfAttackedMeters, isTestData) ');
    end

    switch caseNum 
        case 9            
            %IEEE Case9 DC
            load('IEEE9DC.mat');
            %load case9W bus system
            mpc = loadcase('case9');
            newF=full(TB);
            [rows,cols]=size(newF);
            H=newF(:,2:cols);
            
        case 14             
            %IEEE Case14 DC
            load('IEEE14DC.mat');
            %load case14 bus system
            mpc = loadcase('case14');
            newF=full(TB);
            [rows,cols]=size(newF);
            H=newF(:,2:cols);

        case 30            
            %IEEE Case30 DC
            load('IEEE30DC.mat');
            %load case30 bus system
            mpc = loadcase('case30');
            newF=full(TB);
            [rows,cols]=size(newF);
            H=newF(:,2:cols);
            
        case 118            
            %IEEE Case118 DC
            load('IEEE118DC.mat');
            %load case14 bus system
            mpc = loadcase('case118');
            newF=full(TB);
            [rows,cols]=size(newF);
            H=newF(:,2:cols);
            
        case 300            
            %IEEE Case300 DC
            load('IEEE300DC.mat');
            %load case300 bus system
            mpc = loadcase('case300');  
            newF=full(TB);
            [rows,cols]=size(newF);
            H=newF(:,2:cols);

        otherwise
            mpc = loadcase(strcat('case',num2str(caseNum)))
            H=generate_mete_H(strcat('case',num2str(caseNum)));
    end

    [m,n]=size(H);
    fprintf('Dimension of H is %d x %d\n', m, n );
    fprintf('Number of meters needed for the attack to be undetected by SVE for IEEE-case-%d : %d\n', caseNum, m-n+1 );

    define_constants;

    %normal data
    Z=zeros(numOfSamples, m);
    %normal data with noise
    Z1=zeros(numOfSamples, m);
    
    %generate a series of normal vector z
    mpopt=mpoption('out.all',0, 'model', 'DC');    
    for i=1:numOfSamples
        [results, success]=rundcpf(mpc,mpopt);
        %%disp(success);
        x=results.bus(:,9);
        x=x(2:length(x),1);
        z=(H*x)';
        Z(i,:)=z;

        %with noise    
        e=normrnd(0,0.1,[1 m]);
        Z1(i,:)=z+e;
    end
    
    %{ 
    %%See this for revised code.
     prev_x=zeros(caseNum-1,1);
     i=1;
    while i<=numOfSamples
        results=rundcpf(mpc,mpopt);
        x=results.bus(:,9);
        x=x(2:length(x),1);
        %%TOCHECK
        if not(isequal(prev_x,x)) 
            z=(H*x)';
            Z(i,:)=z;

            %with noise    
            e=normrnd(0,0.1,[1 m]);
            Z1(i, :)=z+e;
            
            prev_x=x;
            i=i+1;
        end
    end
    %}
    
    %generate attack vector a and add that to the normal vector z

    %%note!!! There are different ways to generate attack vector a, which
    %%depends on which kind of attack you want to launch.
    %refer paper: 1) http://arxiv.org/pdf/1502.04254.pdf
    %             2) http://dl.acm.org/citation.cfm?id=1653666
    %              3) http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6032057
    % for details.
        
    A=zeros(numOfSamples, m);
    A_with_noise=zeros(numOfSamples, m);
    A_attackedMeters=zeros(numOfSamples, m);
    
    %Guassian Noise
    i=1;
    while i<=numOfSamples
        disp(i);
        e=normrnd(0,0.1,[ 1 m]);
        a=getAttackVector_sparseTargetedFalseDataInjectionAttack(H, numOfAttackedMeters);
        %%a=getAttackVector_iid(H, numOfAttackedMeters);
        attackedMeters=(a~=0); %%All non-zero values represent attacked meters
        
        if(any(a))
            %%Using Z1 for attack vectors
            a1=Z(i,:)+a';
            A(i,:)=a1;
            A_with_noise(i,:)=a1+e;

            A_attackedMeters(i,:)=attackedMeters;
           % clearvars B_s y a e c;
           i=i+1;
        end
    end    
    attacktype='lasso-iid';
    persistUniqueDataElementsToFile (caseNum,numOfAttackedMeters,attacktype, Z,Z1,A,A_with_noise,A_attackedMeters,isTestData);
    %persistUniqueDataRowsToFile (caseNum,numOfAttackedMeters,attacktype, Z,Z1,A,A_with_noise,A_attackedMeters,isTestData);
    disp('Done!');
end

