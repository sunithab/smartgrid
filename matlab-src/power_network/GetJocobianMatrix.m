function [JocobianMatrix]=GetJocobianMatrix(e,f,BranchMatrix,casenumber)

[p,G,B]=AdmittanceMatrixGenerator(casenumber);
N_busnumber=casenumber;
M_branchnumber=p;
%%%%
H=zeros(N_busnumber,N_busnumber);
N=zeros(N_busnumber,N_busnumber);
M=zeros(N_busnumber,N_busnumber);
L=zeros(N_busnumber,N_busnumber);
R=zeros(N_busnumber,N_busnumber);
S=zeros(N_busnumber,N_busnumber);
X=zeros(N_busnumber,N_busnumber);
Y=zeros(N_busnumber,N_busnumber);
%%%%
P=zeros(M_branchnumber,N_busnumber);
Q=zeros(M_branchnumber,N_busnumber);
Pr=zeros(M_branchnumber,N_busnumber);
Qr=zeros(M_branchnumber,N_busnumber);
P1=zeros(M_branchnumber,N_busnumber);
Q1=zeros(M_branchnumber,N_busnumber);
Pr1=zeros(M_branchnumber,N_busnumber);
Qr1=zeros(M_branchnumber,N_busnumber);

for i=1:N_busnumber
   for j=1:N_busnumber
       [a,b]=Getab(e,f,G,B,i,N_busnumber);
       if(i==j)
       H(i,j)=-a-(G(i,i)*e(i)+B(i,i)*f(i));
       N(i,j)=-b+(B(i,i)*e(i)+G(i,i)*f(i));
       M(i,j)=b+(B(i,i)*e(i)-G(i,i)*f(i));
       L(i,j)=-a+(G(i,i)*e(i)+B(i,i)*f(i));
       R(i,j)=-2*e(i);
       S(i,j)=-2*e(i);
       X(i,j)=-(f(i))/(e(i)*e(i));
       Y(i,j)=-(1/e(i));
       end
       if(i~=j)
       H(i,j)=-(G(i,j)*e(i)+B(i,j)*f(i));
       N(i,j)=B(i,j)*e(i)-G(i,j)*f(i);
       M(i,j)=B(i,j)*e(i)-G(i,j)*f(i);
       L(i,j)=G(i,j)*e(i)+B(i,j)*f(i);
       R(i,j)=0;
       S(i,j)=0;
       X(i,j)=0;
       Y(i,j)=0;
       end
   end
end
J1=horzcat(H,N);
J2=horzcat(M,L);
J3=horzcat(R,S);
J4=horzcat(X,Y);
J5=vertcat(J1,J2,J3,J4);
for j = 1:M_branchnumber
      i=BranchMatrix(j,1);
      k=BranchMatrix(j,2);
      r=BranchMatrix(j,3);
      x=BranchMatrix(j,4);
      g=r/((r*r)+(x*x));
      b=-x/((r*r)+(x*x));
      y=BranchMatrix(j,5);
      P(j,i)=-(2*e(i)-e(k))*g-f(k)*b;
      P(j,k)=e(i)*g+f(i)*b;
      P1(j,i)=-(2*f(i)-f(k))*g+e(k)*b;
      P1(j,k)=f(i)*g-e(i)*b;
      
      Q(j,i)=(2*e(i)-e(k))*b-f(k)*b-e(i)*y;
      Q(j,k)=-e(i)*b+f(i)*g;
      Q1(j,i)=(2*f(i)-f(k))*b+e(k)*g-f(i)*y;
      Q1(j,k)=-f(i)*b-e(i)*g;
      
      Pr(j,i)=e(k)*g+f(k)*b;
      Pr(j,k)=-(2*e(k)-e(i))*g-f(i)*b;
      Pr1(j,i)=f(k)*g-e(k)*b;
      Pr1(j,k)=-(2*f(k)-f(i))*g+e(i)*b;
      
      Q(j,i)=-e(k)*b+f(k)*g;
      Q(j,k)=(2*e(k)-e(i))*b-f(i)*b-e(k)*y;
      Qr1(j,i)=-f(k)*b-e(k)*g;
      Qr1(j,k)=(2*f(k)-f(i))*b+e(i)*g-f(i)*y;
end
J6=horzcat(P,P1);
J7=horzcat(Q,Q1);
J8=horzcat(Pr,Pr1);
J9=horzcat(Qr,Qr1);
J10=vertcat(J6,J7,J8,J9);   
JocobianMatrix=vertcat(J5,J10);
%%
function [a,b]=Getab(e,f,G,B,i,N_busnumber)
  sum1=0;
  sum2=0;
  for k=1:N_busnumber
     sum1=sum1+(e(k)*G(i,k)-f(k)*B(i,k));
     sum2=sum2+(f(k)*G(i,k)+e(k)*B(i,k));
  end
  a=sum1;
  b=sum2;
 %%