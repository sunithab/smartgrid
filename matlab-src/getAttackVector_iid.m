function [a]= getAttackVector_iid(H, numOfAttackedMeters) 
    %generate attack vector a and add that to the normal vector z

    %%note!!! There are different ways to generate attack vector a, which
    %%depends on which kind of attack you want to launch.
    %refer paper: 1) http://arxiv.org/pdf/1502.04254.pdf
    %             2) http://dl.acm.org/citation.cfm?id=1653666
    %              3) http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6032057
    % for details.
        
    %%m-n+1 values need of H need to be compromosed to generate an attack
    %%vector successfully[2]. The attack generation method[1] (section III, method 1) 
    %%used here generates such attacks using sparseness of injection vector 'c'. 
    %%so here we try to vary sparseness of 'c' and try to generate such attack vectors. 
    [m,n]=size(H);
    %%Number of meters to attack cannot be more than number of meters
    assert(numOfAttackedMeters<=m);
    
    rng('shuffle');
    a=rand(m,1);
    %Attacker needs to get access to atleast m-n+1 meters for the attack to
    %be undetected by the StateEstimation
    allColIndxs=1:m;  
    
    unsafeColIndxs = sort(randperm(m, numOfAttackedMeters));
    safeColIndxs = sort(setdiff(allColIndxs,unsafeColIndxs));
    a(safeColIndxs)=0;
end