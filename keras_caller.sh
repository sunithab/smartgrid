folderPath=`pwd`"/data/lasso-iid/"
caseNum="9" #case_9
currFolder=$folderPath'case_'$caseNum
fileName=$currFolder"/training_progress.txt"
echo -e "\n---------------------------------------------------------------------------------\n" >> $fileName

for entry in "$currFolder"/*
do
  if [ -d "$entry" ];then
    numOfAttackedMeters=`echo "$entry" |  xargs -n 1 basename`
    echo $numOfAttackedMeters
    echo "Training: case_$caseNum  with attacked meters: $numOfAttackedMeters" >> $fileName
    SECONDS=0
    (cd python-src && python keras_dl_params_optimizer.py $caseNum $numOfAttackedMeters)
    duration=$SECONDS
    echo "Duration: $(($duration / 60))m $(($duration % 60))s" >> $fileName

  fi
done
