% Give more suggestions for improving the program.

function [result] = predictor(test_set, model, model_name,feature_trans, feature_trans_name )
    result = zeros(length(test_set(:,1)),1);
    k=1;

    %classify test cases
    for j=1:size(test_set,1)
        test_data=test_set(j,:);
            test_out=test_data;
            switch lower(feature_trans_name)
                case 'pca'
                    coefs=feature_trans{k, 1};
                    dims=feature_trans{k, 2};
                    test_out=pca_transform(coefs,dims,test_out);
                case 'mrmr'

                case 'relieff'

                case 'mutinffs'

                case 'fsv'

                case 'laplacian'
                case 'mcfs'

                case 'rfe'
                case 'l0'

                case 'fisher'

                case 'inffs'

                case 'ecfs'

                case 'udfs'

                case 'cfs'   

                case 'llcfs'   

                otherwise
                    %disp('Unknown method.')    

            end

            if(~(strcmp(lower(feature_trans_name), 'none' ) |  strcmp(lower(feature_trans_name), 'pca' )))          
                ranking=feature_trans{k,1};
                num_selected_features = feature_trans{k,2};
                test_out=test_out(:,ranking(1:num_selected_features));
            end

            switch upper(model_name)
                case 'SVM'
                    %val = svmclassify(model, test_out);
                    val = predict(model, test_out);
                case 'KNN'
                    val=model.predict(test_out);
                case'NB' 
                    val=model.predict(test_out);
                case 'BOOST'                    
                    val=predict(model,test_out);
                case 'RF'
                    rng(1); % For reproducibility
                    val=str2double(predict(model,test_out));
                    %str2double(val)
                case 'LOGIS_REG' 
                    [val, prob]=logitBinPred(model, test_out);
            end

        result(j) = val;
    end
end