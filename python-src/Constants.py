ERROR_CODE=-1101
ERROR_PRED_PROB="NONE"

TRAINING_DATA_FILES_PATH="../data/old_data/" #Must end with '/'
ATTACK_LABEL=+1
NO_ATTACK_LABEL=0


TRAINING_DATA_FILE_NAME="training_data.txt"
TRAINING_LABEL_FILE_NAME="training_label.txt"

TRAINING_ATTACK_DATA_FILE_NAME="TrainData_NormalData_IID_case_9_attack_15.txt"
TRAINING_NON_ATTACK_DATA_FILE_NAME="TrainLabel_NormalData_IID_case_9_attack_15.txt"

TEST_DATA_FILE_PATH=TRAINING_DATA_FILES_PATH+"test/"
TEST_DATA_FILE_NAME="test_file.txt"
TEST_DATA_ACTUAL_LABEL_FILE_NAME= "test_result_label_file.txt"

KNN_NEIGHBORS=17

def updateFileNames(caseNum, numOfAttackedMeters) : #Eg: updateFileNames(case_9 , 18)
	global TRAINING_DATA_FILES_PATH
	global TRAINING_DATA_FILE_NAME
	global TRAINING_LABEL_FILE_NAME
	TRAINING_DATA_FILES_PATH="../data/lasso-iid-old/case_"+caseNum+"/"+numOfAttackedMeters+"/" #Must end with '/'

	TRAINING_DATA_FILE_NAME="TrainData_NormalData_IID_case_"+caseNum+"_attack_"+numOfAttackedMeters+".txt"
	TRAINING_LABEL_FILE_NAME="TrainLabel_NormalData_IID_case_"+caseNum+"_attack_"+numOfAttackedMeters+".txt"
	#print TRAINING_DATA_FILE_NAME
