function [Z,RelationMatrix]=GetMeasurements(casenumber)
[p,G,B]=AdmittanceMatrixGenerator(casenumber);
N_busnumber=casenumber;
M_branchnumber=p;
%%%%%%%%%%%%%%%%%%%%%%%to be expanded later%%%%%%%%%%%%%%
[IEEE9_e, IEEE14_e, IEEE30_e, IEEE118_e,IEEE9_f, IEEE14_f, IEEE30_f, IEEE118_f] = Basic_IEEE_Data;
[IEEE9_T, IEEE14_T, IEEE30_T, IEEE118_T] = Basic_IEEE_BranchData;
  if(casenumber==9)
     e=IEEE9_e;
     f=IEEE9_f;
     BranchMatrix=IEEE9_T;
  end
  if(casenumber==14)
     e=IEEE14_e;
     f=IEEE14_f;
     BranchMatrix=IEEE14_T;
  end
  if(casenumber==30)
     e=IEEE30_e;
     f=IEEE30_f;
     BranchMatrix=IEEE30_T;
  end
  if(casenumber==118)
     e=IEEE118_e;
     f=IEEE118_f;
     BranchMatrix=IEEE118_T;
  end
%%%%%%%%%%%%%%%%%%%%%%%to be expanded later%%%%%%%%%%%%%%
offset_f=length(e);
Z1 = zeros(N_busnumber,1);
Z2 = zeros(N_busnumber,1);
Z3 = zeros(N_busnumber,1);
Z4 = zeros(N_busnumber,1);
Z5 = zeros(M_branchnumber,1);
Z6 = zeros(M_branchnumber,1);
Z7 = zeros(M_branchnumber,1);
Z8 = zeros(M_branchnumber,1);
RelationMatrix1=zeros(N_busnumber,2*length(e));
RelationMatrix2=zeros(N_busnumber,2*length(e));
RelationMatrix3=zeros(N_busnumber,2*length(e));
RelationMatrix4=zeros(N_busnumber,2*length(e));
RelationMatrix5=zeros(M_branchnumber,2*length(e));
RelationMatrix6=zeros(M_branchnumber,2*length(e));
RelationMatrix7=zeros(M_branchnumber,2*length(e));
RelationMatrix8=zeros(M_branchnumber,2*length(e));

for i = 1:N_busnumber
   for k=1:N_busnumber
        if(G(i,k)>0)
            RelationMatrix1(i,i)=1;
            RelationMatrix1(i,k)=1;
            RelationMatrix1(i,i+offset_f)=1;
            RelationMatrix1(i,k+offset_f)=1;
        end
        if(B(i,k)>0)
            RelationMatrix1(i,i)=1;
            RelationMatrix1(i,offset_f+k)=1;
            RelationMatrix1(i,offset_f+i)=1;
            RelationMatrix1(i,k)=1;
        end
   end
     for k=1:N_busnumber
        if(G(i,k)>0)
            RelationMatrix2(i,i+offset_f)=1;
            RelationMatrix2(i,k)=1;
            RelationMatrix2(i,i)=1;
            RelationMatrix2(i,k+offset_f)=1;
        end
        if(B(i,k)>0)
            RelationMatrix2(i,i+offset_f)=1;
            RelationMatrix2(i,offset_f+k)=1;
            RelationMatrix2(i,i)=1;
            RelationMatrix2(i,k)=1;
        end
     end
     RelationMatrix3(i,i+offset_f)=1;
     RelationMatrix3(i,i)=1;
     RelationMatrix4(i,i+offset_f)=1;
     RelationMatrix4(i,i)=1;
end
for j = 1:M_branchnumber
    i=BranchMatrix(j,1);
    k=BranchMatrix(j,2);
    r=BranchMatrix(j,3);
    x=BranchMatrix(j,4);
    g=r/((r*r)+(x*x));
    b=-x/((r*r)+(x*x));
    y=BranchMatrix(j,5);
    if(g>0)
       RelationMatrix5(j,i)=1;
       RelationMatrix5(j,k)=1;
       RelationMatrix5(j,i+offset_f)=1;
       RelationMatrix5(j,k+offset_f)=1;
       RelationMatrix6(j,i)=1;
       RelationMatrix6(j,k)=1;
       RelationMatrix6(j,i+offset_f)=1;
       RelationMatrix6(j,k+offset_f)=1;
       RelationMatrix7(j,i)=1;
       RelationMatrix7(j,k)=1;
       RelationMatrix7(j,i+offset_f)=1;
       RelationMatrix7(j,k+offset_f)=1;
       RelationMatrix8(j,i)=1;
       RelationMatrix8(j,k)=1;
       RelationMatrix8(j,i+offset_f)=1;
       RelationMatrix8(j,k+offset_f)=1;
    end
    if(b>0)
       RelationMatrix5(j,i)=1;
       RelationMatrix5(j,i+offset_f)=1;
       RelationMatrix5(j,k+offset_f)=1;
       RelationMatrix5(j,k)=1;
       RelationMatrix6(j,i)=1;
       RelationMatrix6(j,k)=1;
       RelationMatrix6(j,i+offset_f)=1;
       RelationMatrix6(j,k+offset_f)=1;
       RelationMatrix7(j,i)=1;
       RelationMatrix7(j,k)=1;
       RelationMatrix7(j,i+offset_f)=1;
       RelationMatrix7(j,k+offset_f)=1;
       RelationMatrix8(j,i)=1;
       RelationMatrix8(j,k)=1;
       RelationMatrix8(j,i+offset_f)=1;
       RelationMatrix8(j,k+offset_f)=1;
    end
    if(y>0)
       RelationMatrix6(j,i)=1;
       RelationMatrix6(j,i+offset_f)=1;
       RelationMatrix8(j,i)=1;
       RelationMatrix8(j,i+offset_f)=1;
    end
    
end
RelationMatrix=vertcat(RelationMatrix1,RelationMatrix2,RelationMatrix3,RelationMatrix4,RelationMatrix5,RelationMatrix6,RelationMatrix7,RelationMatrix8);
for i = 1:N_busnumber
    sum1=0;
    sum2=0;
    for k=1:N_busnumber
        sum1=sum1+(e(1,k)*G(i,k)-f(1,k)*B(i,k));
        sum2=sum2+(f(1,k)*G(i,k)+e(1,k)*B(i,k));
    end
    Z1(i,1)=-e(1,i)*sum1-f(1,i)*sum2;
    Z2(i,1)=-f(1,i)*sum1+e(1,i)*sum2;
    Z3(i,1)=-(e(1,i)*e(1,i)+f(1,i)*f(1,i));
    Z4(i,1)=atan(f(1,i)/e(1,i));
 
end
for j = 1:M_branchnumber
    i=BranchMatrix(j,1);
    k=BranchMatrix(j,2);
    r=BranchMatrix(j,3);
    x=BranchMatrix(j,4);
    g=r/((r*r)+(x*x));
    b=-x/((r*r)+(x*x));
    y=BranchMatrix(j,5);
    Z5(j,1)=-(e(1,i)*(e(1,i)-e(1,k))+f(1,i)*(f(1,i)-f(1,k)))*g+(e(1,i)*(f(1,i)-f(1,k))-f(1,i)*(e(1,i)-e(1,k)))*b;
    Z6(j,1)=(e(1,i)*(e(1,i)-e(1,k))+f(1,i)*(f(1,i)-f(1,k)))*b+(e(1,i)*(f(1,i)-f(1,k))-f(1,i)*(e(1,i)-e(1,k)))*g-(e(1,i)*e(1,i)+f(1,i)*f(1,i))*(y/2);
    Z7(j,1)=-(e(1,k)*(e(1,k)-e(1,i))+f(1,k)*(f(1,k)-f(1,i)))*g+(e(1,k)*(f(1,k)-f(1,i))-f(1,k)*(e(1,k)-e(1,i)))*b;
    Z8(j,1)=(e(1,k)*(e(1,k)-e(1,i))+f(1,k)*(f(1,k)-f(1,i)))*b+(e(1,k)*(f(1,k)-f(1,i))-f(1,k)*(e(1,k)-e(1,i)))*g-(e(1,k)*e(1,k)+f(1,k)*f(1,k))*(y/2);
end
Z=vertcat(Z1,Z2,Z3,Z4,Z5,Z6,Z7,Z8);

