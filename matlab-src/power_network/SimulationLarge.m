clc
clear

%% Start Simulation
load('IEEE118DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
newF=newF(:,2:cols);
load('XData118.mat');
 x=savedata';
 x(1)=[];
[rows,cols]=size(newF);
k=0;
 y=newF*x;
oldy=y;
fprintf('\nThe matrix H is a %d-by-%d matrix\n',rows,cols);

% for i=1:cols
%     c=zeros(cols,1);
%     c(i)=randint(1,1,100)+1;
%     
%     tic 
%     [attackvector,nonzerosMP1(i)]=Attack_mp_kc(newF,k,c); 
%     timeMP1(i)=toc;
% end
% nonzerosMP1
% timeMP1


avgtime=zeros(1,10);
avgnz=zeros(1,10);
for kk=1:100
   temp=randperm(cols);
for i=1:10
     c=zeros(cols,1);
     for j=1:i
        c(temp(j))=randint(1,1,100)+1;
     end
    tic 
    [attackvector,nonzerosMP(i)]=Attack_mp_kc(newF,k,c); 
    timeMP(i)=toc; 
  %  avgtime(i)=avgtime(i)+timeMP(i);
   % avgnz(i)=avgnz(i)+nonzerosMP(i);
end
end
timeMP
nonzerosMP
%avgtime/100
%avgnz/100


%No constraint on c,  # of non-zeros in attackvector related to the time to
%find such attack vector, # of non-zeros is from 4 to 10
% for k=10:20
%      tic
%      attackvector=Attack_Stack_k(newF,k);
%      timeGreedy(k-9)=toc;
% end
% timeGreedy

%Constraint on c # of affected state variables  related to the time to
%find such attack vector and # of non-zeros in the attack vector and the number of state
% variables is from 1 to 6


 

