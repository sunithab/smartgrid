function [x,tau]=Matching_Pursuit(A,b)
[m,n]=size(A);
r=0;
Ar=A;
for i=1:n
     nr=norm(Ar(:,i));
     for j=1:m
         Ar(j,i)=Ar(j,i)/nr;
     end
end
br=b;
thresthold=0.0001;
tau=[];
for i=1:m
        a(i)=i;
 end
while (norm(br)>=thresthold)
    if (Ar'*br==0)
          fprintf('no solution exists!\n');
          x=zeros(n,1);
          return;
    else
         t=setdiff(a,tau);
         max=abs(Ar(:,t(1))'*br);
         max_ar=Ar(:,t(1));
         max_k=t(1);
         for k=1:length(t)
               if (abs(Ar(:,t(k))'*br)>max)
                      max=abs(Ar(:,t(k))'*br);
                      max_ar=Ar(:,t(k));
                      max_k=t(k);
               end
         end
         br=br-(max_ar'*br)*max_ar;
         tau=horzcat(tau,max_k);
         t=setdiff(a,tau);
         for j=1:length(t)
             Ar(:,t(j))=Ar(:,t(j))-(max_ar'*Ar(:,t(j)))*max_ar;
             Ar(:,t(j))=Ar(:,t(j))/norm(Ar(:,t(j)));
         end
    end
    r=r+1;
%     if(r==55)
%        break;
%     end
end
B=[];
for i=1:length(tau)
       B=horzcat(B,A(:,tau(i)));    
end
 I=eye(size(pinv(B,1e-5)*B));
 [mI,mI]=size(I);
 z=ones(mI,1);
 x=pinv(B,1e-5)*(b-br)+(I-pinv(B,1e-5)*B)*z;    
