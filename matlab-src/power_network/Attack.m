function [attackvector]=Attack(H)
[rows,cols]=size(H);
middlematrix=H;
m=rows;
n=cols;
appendmatrix=zeros(m,1);
permutation=randperm(m);
for i=1:n-1
   zero_position(i)=permutation(i);
end
for i=1:m-n+1
   attack_position(i)=permutation(n-1+i);
end
for i=1:n-1
    [mm,nn]=size(middlematrix);
    if(nn==1)
         break;
    end
    basis=middlematrix(:,1);
    appendmatrix=zeros(m,1);
   for j=2:nn%n-i+1
       temp=middlematrix(zero_position(i),j);
      if (abs(temp)<=1e-10)
          appendvector=middlematrix(:,j);
      else
          if(abs(basis(zero_position(i)))<=1e-10)
              appendvector=basis;
              tempvector=basis;
              basis=middlematrix(:,j);
              middlematrix(:,j)=tempvector;
          else   
             appendvector=middlematrix(:,j)/temp*basis(zero_position(i));
             tempvector=appendvector;
             appendvector=basis-tempvector;
          end
      end
       if (sum(abs(appendmatrix(:,1)))<=1e-10)
           appendmatrix=appendvector;
       else
           appendmatrix=horzcat(appendmatrix,appendvector);
       end
   end
   middlematrix=appendmatrix;
end
attackvector=middlematrix;

