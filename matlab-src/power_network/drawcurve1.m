clc
clear

LabelFontSize=16;

% %%%%%%%%%%% 9busvstimeHeurBF %%%%%%%%%%%%%%%%%%%%%
% x=[ 4          5         6         7        8         9          10];
% y1=[0.0010    0.0002    0.0002    0.0002    0.0002    0.0002    0.0002];
% y2=[0.0432    0.0293    0.0107    0.0100    0.0092    0.0073    0.0058];
% semilogy(x,y1,'ko-')
% hold on
% semilogy(x,y2,'k+-')
% hold off
% xlabel('# of meters to compromise');
% ylabel('time(seconds)');
% legend('Heuristic Algorithm', 'Brute-force Algorithm');
% grid on
% %%%%%%%%%%%%% 300118busvstime %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for i=10:20
%     x(i-9)=i;
% end
% y1=[21.7479   21.5327   21.5336    0.0361    0.0360    0.0356    0.0364    0.0365    0.0359  0.0363    0.0361];
% y2=[0.0099    0.0122    0.0099    0.0088    0.0121    0.0086    0.0104    0.0094    0.0094 0.0098    0.0123];
% %figure
% semilogy(x,y1,'ko-')
% hold on
% semilogy(x,y2,'k+-')
% hold off
% xlabel('# of meters to compromise');
% ylabel('time(seconds)');
% legend('IEEE300bus', 'IEEE118bus');
% grid on
% %%%%%%%%%%%%%%% 300118busvstargestime %%%%%%%%%%%%%%%%%%%%%%%%%%
% x=zeros(1,10);
% for i=1:10
%     x(i)=i;
% end
% y1=[1.2740    2.1184    2.9273    3.5959    4.4385    5.0901    5.9011    6.5064    7.2794  7.8732];
% y2=[0.2897    0.4762    0.6988    0.8536    1.0108    1.1664    1.2984    1.4552    1.5713   1.6919];
% %y3=[0.0179    0.0346    0.0465    0.0586    0.0672    0.0739    0.0813    0.0884    0.0938 0.0986];
% %y4=[0.0107    0.0165    0.0212    0.0251    0.0286    0.0309    0.0328    0.0351    0.0369  0.0384];
% %figure
% semilogy(x,y1,'ko-')
% hold on
% semilogy(x,y2,'k+-')
% % hold on
% % semilogy(x,y3,'k*-')
% % hold on
% % semilogy(x,y4,'k.-')
% hold off
% xlabel('Number of specific target state variables');
% ylabel('Average computation time(seconds)');
% legend('IEEE300bus', 'IEEE118bus');
% grid on
% %%%%%%%%%%%%%%% 300118busvstargesnz %%%%%%%%%%%%%%%%%%%%%%%%%%
% x=zeros(1,10);
% for i=1:10
%     x(i)=i;
% end
% y1=[11.0600   21.8700   32.3900   41.0700   51.9800   60.4200   71.2900   79.2500   89.7600   97.8200];
% y2=[13.3600   23.5400   36.0200   45.0900   54.2700   63.5600   71.1000   80.9700   87.9800   95.4900];
% %y3=[8.1700   17.0600   23.7200   30.7100   35.8700   40.2200   44.9500   49.3800   53.5500 57.0300];
% %y4=[8.2900   16.0400   21.3800   26.0400   30.6100   34.2300   37.6600   40.8300   43.8600  46.7500];
% %figure
% plot(x,y1,'ko-')
% hold on
% plot(x,y2,'k+-')
% % hold on
% % plot(x,y3,'k*-')
% % hold on
% % plot(x,y4,'k.-')
% hold off
% xlabel('Number of specific target state variables');
% ylabel('Average number of meters to compromise');
% legend('IEEE300bus', 'IEEE118bus');
% grid on
% grid on
% %%%%%%%%%%%%%%% 300118busvstargesnzzero %%%%%%%%%%%%%%%%%%%%%%%%%%
% x=zeros(1,10);
% for i=1:10
%     x(i)=i;
% end
% y1=[10.8800   20.9900   31.9500   41.5700   50.6800   59.6300   69.2700   78.4000   86.5200   94.7300];
% y2=[10.7700   20.6000   30.7400   41.0700   50.4000   59.9100   69.2700   77.8700   87.2200   95.8900];
% %y3=[ 9.9100   18.6500   26.9600   34.6300   41.6800   48.3000   54.3700   59.8000   65.2300  70.2600];
% %y4=[10.1800   18.1600   24.8400   31.3100   36.6100   40.7300   44.3600   46.8900   49.3200 51.1700];
% %figure
% plot(x,y1,'ko-')
% hold on
% plot(x,y2,'k+-')
% % hold on
% % plot(x,y3,'k*-')
% % hold on
% % plot(x,y4,'k.-')
% hold off
% xlabel('Number of specific target state variables');
% ylabel('Average number of meters to compromise');
% legend('IEEE300bus', 'IEEE118bus');
% grid on
% 
% %%%%%%%%%%%%%%% 300indicenzzero%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('Bus300.mat');
% x=zeros(1,299);
% y=zeros(1,299);
% for i=1:299
%     x(i)=i+1;
%     y(i)=rows-zerocounter(newF(:,i));
% end
% %figure
% plot(x,y)
% xlabel('Indice of specific target state variables(IEEE300bus)');
% ylabel('Number of meters to compromise');
% grid on
% %%%%%%%%%%%%%%% 118indicenzzero%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('118bus.mat');
% size(newF)
% x=zeros(1,117);
% y=zeros(1,117);
% for i=1:117
%     x(i)=i+1;
%     y(i)=rows-zerocounter(newF(:,i));
% end
% %figure
% plot(x,y)
% xlabel('Indice of specific target state variables(IEEE118bus)');
% ylabel('Number of meters to compromise');
% grid on
% %%%%%%%%%%%%%%%%% baravgnzzero%%%%%%%%%%%%%%%%%%%%%%%%%
% xx=[ 9, 14 30 118 300];
% load('Bus300.mat');
% t=0
% for i=1:cols
%     t=t+rows-zerocounter(newF(:,i));
% end
% avgnz0(1)=t/cols;
% 
% load('118bus.mat');
% t=0
% for i=1:cols
%     t=t+rows-zerocounter(newF(:,i));
% end
% avgnz0(2)=t/cols;
% 
% load('30bus.mat');
% t=0
% for i=1:cols
%     t=t+rows-zerocounter(newF(:,i));
% end
% avgnz0(3)=t/cols;
% 
% load('14bus.mat');
% t=0
% for i=1:cols
%     t=t+rows-zerocounter(newF(:,i));
% end
% avgnz0(4)=t/cols;
% 
% load('9bus.mat');
% t=0
% for i=1:cols
%     t=t+rows-zerocounter(newF(:,i));
% end
% avgnz0(5)=t/cols;
% 
% avgnz0
% 
% y=[0.0082   0.021158    0.08282  0.18091   0.27315];
% figure
% bar(y);
% set(gca,'XTickLabel',  {'300 bus'; '118 bus'; '30 bus';'14bus';'9bus'} );
% ylabel('Average percentage of  the meters to compromise');
% grid on
% 
% %%%%%%%%%%%%%%% 300indicenz%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('Bus300.mat');
% x=zeros(1,299);
% for i=1:299
%     x(i)=i+1;
% end
% y=nonzerosMP1;
% figure
% plot(x,y)
% xlabel('Indice of target state variables(IEEE300bus)');
% ylabel('# of meters to compromise');
% %legend('Matching Pursuit Algorithm');
% grid on
% %%%%%%%%%%%%%%%% 118indicetime %%%%%%%%%%%%%%%%%%%%%%%%%%
% load('118bus.mat');
% x=zeros(1,117);
% for i=1:117
%     x(i)=i+1;
% end
% y=timeMP1;
% %figure
% plot(x,y)
% xlabel('Indice of target state variables(IEEE118bus)');
% ylabel('time(seconds)');
% %legend('Matching Pursuit Algorithm');
% grid on
% 
% %%%%%%%%%%%%%%%%% 118indicenz %%%%%%%%%%%%%%%%%%%%%%%%%
% load('118bus.mat');
% x=zeros(1,117);
% for i=1:117
%     x(i)=i+1;
% end
% y=nonzerosMP1;
% %figure
% plot(x,y)
% xlabel('Indice of target state variables(IEEE118bus)');
% ylabel('# of meters to compromise');
% %legend('Matching Pursuit Algorithm');
% grid on
% %%%%%%%%%%%%%%%%% baravgtime%%%%%%%%%%%%%%%%%%%%%%%%%
% xx=[ 9, 14 30 118 300];
% load('Bus300.mat');
% avgtime(1)=sum(timeMP1)/299;
% avgnz(1)=sum(nonzerosMP1)/299;
% load('118bus.mat');
% avgtime(2)=sum(timeMP1)/117;
% avgnz(2)=sum(nonzerosMP1)/117;
% load('30bus.mat');
% avgtime(3)=sum(timeMP1)/29;
% avgnz(3)=sum(nonzerosMP1)/29;
% load('14bus.mat');
% avgtime(4)=sum(timeMP1)/13;
% avgnz(4)=sum(nonzerosMP1)/13;
% load('9bus.mat');
% avgtime(5)=sum(timeMP1)/8;
% avgnz(5)=sum(nonzerosMP1)/8;
% avgtime
% avgnz
% y=[0.00968   0.02848    0.07758   0.15242   0.2731];
% %figure
% bar(avgtime);
% set(gca,'XTickLabel', {'300 bus'; '118 bus'; '30 bus';'14bus';'9bus'} );
% ylabel('time(seconds)');
% grid on
% %%%%%%%%%%%%%%%%%barpercentagenz%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %figure
% bar(y);
% set(gca,'XTickLabel',  {'300 bus'; '118 bus'; '30 bus';'14bus';'9bus'} );
% ylabel('Percentage of  the meters to compromise');
% grid on
% %%%%%%%%%%%%%%%%% 118propdouble%%%%%%%%%%%%%%%%%%%%%%%%%
% load('Prop118.mat'); 
% for i=1:373
%    x_118prop(i)=i;
%    y_118Prop(i)=counter(i)/100;
% end
% for i=374:490
%    x_118prop(i)=i;
%    y_118Prop(i)=1;
% end
% figure
% plot(x_118prop, y_118Prop)
% hold on
% load('10prop118.mat'); 
% for i=1:490
%    x_10118prop(i)=i;
%    y_10118Prop(i)=counterc(i)/1000;
% end
% %figure
% plot(x_10118prop, y_10118Prop)
% hold off
% xlabel('Number of specific meters to compromise (IEEE118bus)');
% ylabel('Probability of finding the attack vector');
% grid on
% %%%%%%%%%%%%%%%%% 118propc%%%%%%%%%%%%%%%%%%%%%%%%%
% for i=1:490
%     y_118Propc(i)=counterc(i)/100;
% end
% figure
% plot(x_118prop, y_118Propc)
% xlabel('Number of specific meters to compromise');
% ylabel('Probability of finding the attack vector');
% grid on
% %%%%%%%%%%%%%%%%%%%10118propc%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load('10prop118.mat'); 
% for i=1:490
%    x_10118prop(i)=i;
%    y_10118Prop(i)=counterc(i)/1000;
% end
% figure
% plot(x_10118prop, y_10118Prop)
% xlabel('Number of specific meters to compromise');
% ylabel('Probability of finding the attack vector');
% grid on
% %%%%%%%%%%%%%%%%% 30propdouble%%%%%%%%%%%%%%%%%%%%%%%%%
% load('Prop30.mat'); 
% for i=1:83
%    x_30prop(i)=i;
%    y_30Prop(i)=counter(i)/1000;
% end
% for i=84:112
%    x_30prop(i)=i;
%    y_30Prop(i)=1;
% end
% figure
% plot(x_30prop, y_30Prop)
% hold on
% %%%%%%%%%%%%%%%%%%%1030propc%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for i=1:112
%    x_1030prop(i)=i;
%    y_1030Prop(i)=counterc(i)/10000;
% end
% %figure
% plot(x_1030prop, y_1030Prop)
% hold off
% xlabel('Number of specific meters to compromise (IEEE30bus)');
% ylabel('Probability of finding the attack vector');
% grid on
%%%%%%%%%%%%%%%%%%%30118random%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Prop9.mat'); 
for i=1:19
   x_9prop(i)=i/27;
   y_9prop(i)=counter(i)/1000;
end
for i=20:27
   x_9prop(i)=i/27;
   y_9prop(i)=1;
end
figure
plot(x_9prop, y_9prop,'--.')
hold on

load('Prop14.mat'); 
for i=1:41
   x_14prop(i)=i/53;
   y_14prop(i)=counter(i)/1000;
end
for i=42:53
   x_14prop(i)=i/53;
   y_14prop(i)=1;
end
% newy=smooth(x_14prop,y_14prop,0.1,'rloess');
% plot(x_14prop,newy,'--')
plot(x_14prop, y_14prop,'-..')
hold on

load('Prop30.mat'); 
for i=1:83
   x_30prop(i)=i/112;
   y_30prop(i)=counter(i)/1000;
end
for i=84:112
   x_30prop(i)=i/112;
   y_30prop(i)=1;
end
%newy=smooth(x_30prop,y_30prop,0.1,'rloess');
%plot(x_30prop,newy,'-.')
plot(x_30prop, y_30prop,'-.')
hold on

load('Prop118.mat'); 
for i=1:373
   x_118prop(i)=i/490;
   y_118prop(i)=counter(i)/100;
end
for i=374:490
   x_118prop(i)=i/490;
   y_118prop(i)=1;
end
%newy=smooth(x_118prop,y_118prop,0.1,'rloess');
%plot(x_118prop,newy,':')
plot(x_118prop, y_118prop,'--r')
hold on

load('Prop300prev.mat'); 
for i=1:823
   x_300prop(i)=i/1122;
   y_300prop(i)=counter(i)/100;
end
for i=824:1122
   x_300prop(i)=i/1122;
   y_300prop(i)=1;
end
%newy=smooth(x_300prop,y_300prop,0.1,'rloess');
%plot(x_300prop,newy)
plot(x_300prop, y_300prop)
hold off
legend('9-bus','14-bus', '30-bus', '118-bus','300-bus');
set(gca,'fontsize',LabelFontSize);
%axis([0 7 0 20])
xlabel('Percentage of specific meters to compromise','fontsize', LabelFontSize);
ylabel('Probability of finding the attack vector','fontsize', LabelFontSize);
grid on

print -depsc2 -tiff figures/30118random.eps

%%%%%%%%%%%%%%%%%%%30118target%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Prop9.mat'); 
for i=1:27
   x_109prop(i)=i/27;
   y_109Prop(i)=counterc(i)/8000;
end
figure
plot(x_109prop, y_109Prop,'--.')
hold on

load('Prop14.mat'); 
for i=1:53
   x_1014prop(i)=i/53;
   y_1014Prop(i)=counterc(i)/10000;
end
plot(x_1014prop, y_1014Prop,'-..')
hold on

load('Prop30.mat'); 
for i=1:112
   x_1030prop(i)=i/112;
   y_1030Prop(i)=counterc(i)/10000;
end
plot(x_1030prop, y_1030Prop,'-.')
hold on

load('10prop118.mat'); 
for i=1:490
   x_10118prop(i)=i/490;
   y_10118Prop(i)=counterc(i)/1000;
end
plot(x_10118prop, y_10118Prop,'--r')
hold on

load('Prop300mydesktop.mat'); 
s1=counterc;
load('Prop300prev.mat'); 
s2=counterc;
load('Prop300home.mat'); 
s3=counterc;
load('Prop300eps1.mat'); 
s4=counterc;
load('Prop300eps2.mat');
s5=counterc;
% load('Prop300eps3.mat');
% s6=counterc;
% load('Prop300eps4.mat');
% s7=counterc;
s=s1+s2+s3+s4+s5;
for i=1:1122
   x_10300prop(i)=i/1122;
   y_10300Prop(i)=s(i)/200;
end
plot(x_10300prop, y_10300Prop)
hold off

legend('9-bus', '14-bus', '30-bus', '118-bus','300-bus');
set(gca,'fontsize',LabelFontSize);
xlabel('Percentage of specific meters to compromise','fontsize', LabelFontSize);
ylabel('Probability of finding the attack vector','fontsize',LabelFontSize);
grid on
print -depsc2 -tiff figures/30118target.eps
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%samplecon%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% target=[10 8 9 8 9 11]';
% figure
% bar(target);
% axis([0 7 0 20]);
% set(gca,'XTickLabel', {'set 1'; 'set 2'; 'set 3'; 'set 4';'set 5';'set 6'} );
% set(gca,'fontsize',LabelFontSize);
% ylabel('Number of target state variables','fontsize',LabelFontSize);
% grid on
% print -depsc2 -tiff figures/samplecon.eps
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%samplecon300%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
% target=[13 18 18 14 13 18]';
% figure
% bar(target);
% axis([0 7 0 20])
% set(gca,'XTickLabel', {'set 1'; 'set 2'; 'set 3'; 'set 4';'set 5';'set 6'} );
% set(gca,'fontsize',LabelFontSize);
% ylabel('Number of target state variables','fontsize',LabelFontSize);
% grid on
% print -depsc2 -tiff figures/samplecon300.eps



load('SIRandom300(1).mat'); 
for i=1:823
   x_300prop(i)=i/1122;
   y_300prop(i)=counter(i)/100;
end
for i=824:1122
   x_300prop(i)=i/1122;
   y_300prop(i)=1;
end
plot(x_300prop, y_300prop,'r')
hold on
for i=1:823
   y_300prop1(i)=counter_e(i)/100;
end
for i=824:1122
   y_300prop1(i)=1;
end
plot(x_300prop, y_300prop1)
hold off
legend('Prevous Curve','Current Curve');
set(gca,'fontsize',LabelFontSize);
xlabel('Percentage of specific meters to compromise','fontsize', LabelFontSize);
ylabel('Probability of finding the attack vector','fontsize', LabelFontSize);
grid on
