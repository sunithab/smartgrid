 clc
clear

%% Start Simulation
load('IEEE300DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
newF=newF(:,2:cols);
load('XData300.mat');
 x=savedata';
 x(1)=[];
[rows,cols]=size(newF);

 y=newF*x;
oldy=y;
fprintf('\nThe matrix H is a %d-by-%d matrix\n',rows,cols);

%No constraint on c,  # of non-zeros in attackvector related to the time to
%find such attack vector, # of non-zeros is from 4 to 10
k=4;
tic
attackvector=Attack_Stack_k(newF,k);
toc
%timeGreedy(k-3)=toc;

    
 %   attackvector=Attack_Bru_k(newF,k);
  %  timeBru(k-3)=toc;
%timeGreedy
%timeBru

% c=[0 0 39 0 75 74 0 65]
% [attackvector,nonzerosMP(1)]=Attack_mp_kc(newF,k,c); 
% nonzerosMP

%Constraint on c # of affected state variables  related to the time to
%find such attack vector and # of non-zeros in the attack vector and the number of state
% variables is from 1 to 6
temp=randperm(cols);
for i=1:6 
     c=zeros(cols,1);
     for j=1:i
        c(temp(j))=randint(1,1,100);
     end
    
     tic 
     [attackvector,nonzerosBru(i)]=Attack_Bru_c(newF,c);
     timeBruc(i)=toc;
     attackvector
     y=oldy+attackvector;
     x_bad=inv(newF'*newF)*newF'*y;
     x_bad-x
     consistency=norm(y-newF*inv(newF'*newF)*newF'*y)
  
     tic 
    %change [x,tau]=Matching_Pursuit_k(P,y,k) in Attack_mp_kc to
    % [x,tau]=Matching_Pursuit(P,y) to find the near optimal solution
    [attackvector,nonzerosMP(i)]=Attack_mp_kc(newF,k,c); 
    timeMP(i)=toc;
    attackvector
    y=oldy+attackvector;
    x_bad=inv(newF'*newF)*newF'*y;
    x_bad-x
    consistency=norm(y-newF*inv(newF'*newF)*newF'*y)
end
timeBruc
timeMP
nonzerosBru
nonzerosMP
