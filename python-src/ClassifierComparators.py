import SVMClassifier as svm
import Constants
import numpy as np
import ClassifierUtils as cu
import AttackFeatureClassifier as afc

class ClassifierComparators :
    def __init__(self):
        self.classifier= None
        self.featureDetector = None

    def get_training_data(self):
        train_attack_data = cu.get_file_data(Constants.TRAINING_DATA_FILES_PATH + Constants.TRAINING_ATTACK_DATA_FILE_NAME,
                                         delimiter=",", dtype=float)
        train_no_attack_data = cu.get_file_data(
            Constants.TRAINING_DATA_FILES_PATH + Constants.TRAINING_NON_ATTACK_DATA_FILE_NAME,
            delimiter=",", dtype=float)

        # Combine both attack and non-attack data
        trainingData = np.vstack((train_attack_data, train_no_attack_data))

        y_temp = []
        for i in range(0, len(train_attack_data)):
            y_temp.append(Constants.ATTACK_LABEL)
        for i in range(0, len(train_no_attack_data)):
            y_temp.append(Constants.NO_ATTACK_LABEL)
        # print len(lc_data), len(no_lc_data), len(y_temp)

        trainingDataLabel = np.asarray(y_temp, dtype=float)
        return trainingData, trainingDataLabel

    def get_prediction_counts (self,  actualResults, predictionResults) :
        truePos = 0
        trueNeg = 0
        falsePos = 0
        falseNeg = 0
        for actualRes, predictionValue in zip(actualResults, predictionResults):
            predictionRes = predictionValue[0]
            if actualRes == Constants.ATTACK_LABEL :
                if predictionRes == Constants.ATTACK_LABEL :
                    truePos += 1
                elif predictionRes == Constants.NO_ATTACK_LABEL :
                    falsePos += 1

            elif actualRes == Constants.NO_ATTACK_LABEL :
                if predictionRes == Constants.ATTACK_LABEL:
                    falsePos += 1
                elif predictionRes == Constants.NO_ATTACK_LABEL:
                    trueNeg += 1
        return (truePos, trueNeg, falsePos, falseNeg)

    def get_predictions_with_attacked_features(self,  testDataFile, testDataActualResultsFile):
        truePos =trueNeg = falsePos= falseNeg= 0
        actualResults= cu.get_file_data(testDataActualResultsFile, dtype=int)
        testData= cu.get_file_data(testDataFile)
        predictionResults = []
        for testDataRow in testData :
            predResult, predProb = self.classifier.get_prediction_result(testDataRow)
            indices = []
            if predResult == Constants.ATTACK_LABEL :
                #Find attacked labels
                feaPredResult, feaPredProb = self.featureDetector.get_prediction_result(testDataRow)
                if Constants.ATTACK_LABEL in  feaPredResult :
                    indices = [i for i, x in enumerate(feaPredResult) if x == Constants.ATTACK_LABEL]
            predictionResults.append((predResult, predProb, indices))

        (tp, tn, fp, fn) = self.get_prediction_counts(actualResults, predictionResults)
        truePos += tp
        trueNeg += tn
        falsePos += fp
        falseNeg += fn
        #print truePos, trueNeg, falsePos, falseNeg
        cu.get_classifier_metrics(tp=truePos, tn=trueNeg, fp=falsePos, fn=falseNeg)
        self.display_result(predictionResults)

        return (truePos, trueNeg, falsePos, falseNeg)

    def display_result(self, results):
        for result, acc, attackedFeatures in results:
            if result == Constants.ATTACK_LABEL :
                print "Attack data.. \t PredictionProb: ", acc , "\t AttackedFeatures: ", attackedFeatures
            elif result == Constants.NO_ATTACK_LABEL :
                print "Normal Data.. No attack. \t PredictionProb: ", acc
            else :
                print Constants.ERROR_CODE , "No Accuracy"


    def get_predictions(self, testDataFile, testDataActualResultsFile):
        truePos =trueNeg = falsePos= falseNeg= 0

        actualResults= cu.get_file_data(testDataActualResultsFile, dtype=int)
        predictionResults = self.classifier.get_predictions(testDataFile)
        (tp, tn, fp, fn) = self.get_prediction_counts(actualResults, predictionResults)
        truePos += tp
        trueNeg += tn
        falsePos += fp
        falseNeg += fn

        self.classifier.display_result(predictionResults)
        cu.get_classifier_metrics(tp=truePos, tn=trueNeg, fp=falsePos, fn=falseNeg)
        '''
        while True:
            testFileName = raw_input("Provide test filename with path or press ctrl+c to exit: ")
            actualPredictionFileName = raw_input("Provide test filename with actual result values with path :")
            actualResults = self.get_file_data(actualPredictionFileName, dtype=int)
            predictionResults = classifier.get_prediction_result(testFileName)
            (tp, tn, fp, fn) = self.get_prediction_counts(actualResults, predictionResults)
            truePos += tp
            trueNeg += tn
            falsePos += fp
            falseNeg += fn
            classifier.display_result(predictionResults)
            self.get_classifier_metrics(tp=truePos, tn=trueNeg, fp=falsePos, fn=falseNeg)
        '''
        return  ( truePos, trueNeg, falsePos, falseNeg)


if __name__ == '__main__':
    print "Starting training"
    testFileName=Constants.TEST_DATA_FILE_PATH+ Constants.TEST_DATA_FILE_NAME;
    testResultLabelFileName=Constants.TEST_DATA_FILE_PATH+ Constants.TEST_DATA_ACTUAL_LABEL_FILE_NAME;

    cc = ClassifierComparators()
    trainingData, trainingDataLabels= cc.get_training_data()

    cc.classifier = svm.SVMClassifier(trainingData, trainingDataLabels)
    #cc.classifier = afc.AttackFeatureClassifier(trainingData, trainingDataLabels)
    cc.get_predictions( testDataFile=testFileName, testDataActualResultsFile=testResultLabelFileName)

    #cc.featureDetector = afc.AttackFeatureClassifier(trainingData, trainingDataLabels)
    #cc.get_predictions_with_attacked_features(testDataFile=testFileName, testDataActualResultsFile=testResultLabelFileName)
