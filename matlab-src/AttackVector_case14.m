%IEEE Case14 DC
load('IEEE14DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
H=newF(:,2:cols);
[m,n]=size(H);

%load case14 bus system
mpc = loadcase('case14');
define_constants;

%how many data you want to generate.
length = 15;
%normal data
Z=zeros(m,length);
%normal data with noise
Z1=zeros(m,length);

%generate a series of normal vector z
mpopt=mpoption('out.all',0);
for i=1:length
    results=rundcpf(mpc,mpopt);
    x=results.bus(:,9);
    x=x(2:length(x),1);
    z=H*x;
    Z(:,i)=z;
    
    %with noise    
    e=normrnd(0,0.1,[m 1]);
    Z1(:,i)=z+e;
end
%TODO disp(Z)
dataPath = fullfile( cd(cd('..')), 'data');
%dataPath = fullfile( dataPath, 'test');

dlmwrite(fullfile(dataPath, 'NormalData.txt'), Z', '-append');
disp('Generated normal data without noise');

dlmwrite(fullfile(dataPath, 'NormalDataWithNoise.txt'),Z1', '-append');
disp('Generated normal data with noise');

%generate attack vector a and add that to the normal vector z

%%note!!! There are different ways to generate attack vector a, which
%%depends on which kind of attack you want to launch.
%refer paper: 1) http://arxiv.org/pdf/1502.04254.pdf
%             2) http://dl.acm.org/citation.cfm?id=1653666
%              3) http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6032057
% for details.

%Attacked data Based on paper-1 above
A1=zeros(m,length);
A1_with_noise=zeros(m,length);
numOfAttackedMeters=0;
A1_attackedMeters=zeros(numOfAttackedMeters,length);

%Guassian Noise
for i=1:length
    disp(i);
    e=normrnd(0,0.1,[m 1]);
    % make z is not linear dependent with each other
    c=rand(n,1)*i;
    [B_s,y,attackedMeters] = getProjectionMatrices(H,c,numOfAttackedMeters);
    a = getAttackVectorCandidate(B_s, y, H, c) ;
    a1=Z(:,i)+a;
    A1(:,i)=a1;
    A1_with_noise(:,i)=a1+e;
    A1_attackedMeters(:,i)=attackedMeters;
   % clearvars B_s y a e c;
end
formatSpec = 'AttackData_%d%s.txt';
formatSpecMeters = 'AttackMeters_%d.txt';

dlmwrite(fullfile(dataPath, sprintf(formatSpec,numOfAttackedMeters,'')),A1', '-append');
disp('Generated attack data without noise');

dlmwrite(fullfile(dataPath, sprintf(formatSpec,numOfAttackedMeters,'_noise')),A1_with_noise', '-append');
dlmwrite(fullfile(dataPath, sprintf(formatSpecMeters,numOfAttackedMeters)),A1_attackedMeters', '-append');

disp('Generated attack data with noise');
disp('Done!');
% Z1 is a matrix that contains a series of attacked data.
