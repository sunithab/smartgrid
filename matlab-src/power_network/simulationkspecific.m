clc
clear

%% Start Simulation
trailnum=1000;
load('IEEE9DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
%%%%%%%%%%%%%%%print the matrix to a file%%%%%%%%%%%%
% fid = fopen('Displaymatrix.txt', 'wt');
% for i=1:rows
%     for j=1:cols-1
%        fprintf(fid, ' %.2f &', newF(i,j));
%     end
%    fprintf(fid, '%.2f \\\\ \n',newF(i,cols));
% end    
% fclose(fid);
newF=newF(:,2:cols);
load('XData9.mat');
 x=savedata';
 x(1)=[];
[rows,cols]=size(newF);
k=0;
 y=newF*x;
oldy=y;
fprintf('\nThe matrix H is a %d-by-%d matrix\n',rows,cols);
H=newF;
I=eye(rows,rows);
P=H*inv(H'*H)*H'-I;
counter=zeros(1,rows-cols);
fprintf('no constrains on c\n');
for iteration=1:trailnum
  fprintf('%d->',iteration);
for i=1:rows-cols
   positionset=randperm(rows);
   specificpos=zeros(1,i);
   c=zeros(1,i);
   for j=1:i
       specificpos(j)=positionset(j);
   end 
   c=specificpos;
   length_c=length(c);
   A=P(:,c(1));
   if(length_c>=2)
        for ii=2: length_c
              A=horzcat(A,P(:,c(ii)));
         end
   end
    [a,b]=size(A);
    if(rank(A,1e-5)<min(a,b))%  if(rank(A)<min(a,b))
          counter(i)=counter(i)+1;    
    end
 end
end
counter;



counterc=zeros(1,rows);
svarposset=randperm(cols);
for svarpos=1:8 %for svarpos=1:10%for svarpos=1:cols
    cc=zeros(cols,1); 
    cc(svarposset(svarpos))=100;
 %   cc(svarpos)=100;%randint(1,1,100)+1;
temp=newF;
b=zeros(rows,1);
offset=0;
for i=1:length(cc)
   if(cc(i)~=0)
        b=b+cc(i)*temp(:,i-offset);
        temp(:,i-offset)=[];
        offset=offset+1;
   end
end
H=temp;
P=H*inv(H'*H)*H'-I;
y=P*b;
fprintf('\n constrains on c %d\n',svarpos);
for iteration=1:trailnum
     fprintf('%d->',iteration);
for i=1:rows
   fprintf('%d->%d\n',iteration,i);
   positionset=randperm(rows);
   specificpos=zeros(1,i);
   c=zeros(1,i);
   for j=1:i
       specificpos(j)=positionset(j);
   end 
   c=specificpos;
   length_c=length(c);
   A=P(:,c(1));
   if(length_c>=2)
        for ii=2: length_c
              A=horzcat(A,P(:,c(ii)));
         end
   end
    [a,b]=size(A);
    if(rank(A,10e-5)==rank(horzcat(A,y),10e-5))
             counterc(i)=counterc(i)+1;     
    end
end
end
end
counterc;
