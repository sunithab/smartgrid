import numpy as np
from sklearn.svm import SVC
import Constants
import ClassifierUtils as cu
class SVMClassifier :

    def __init__(self, trainingData, trainingDataLabel):
        self.classifier = self.get_model(trainingData, trainingDataLabel)

    def get_model(self, trainingData, trainingDataLabel):

        self.classifier = SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
            decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
            max_iter=-1, probability=True, random_state=None, shrinking=True,
            tol=0.001, verbose=False)

        self.classifier.fit(trainingData, trainingDataLabel)
        return self.classifier


    def get_prediction_result(self, test_data ):
        result = self.classifier.predict([test_data])
        #print "score: ", self.classifier.score([test_data], result)
        prediction_probability=self.classifier.predict_proba([test_data])
        #print "result", result
        if result[0] == Constants.ATTACK_LABEL :
            return (result[0], str(prediction_probability[0][1]*100))
        elif result[0] == Constants.NO_ATTACK_LABEL :
            return (result[0] , str(prediction_probability[0][0]*100))
        else :
            return (Constants.ERROR_CODE ,Constants.ERROR_PRED_PROB)

    def get_predictions(self, testFileName):
        test_data = cu.get_file_data(testFileName, delimiter=",",dtype=float)
        result=[]
        print "Predicting new data result for smart grid data (normal/attack) : "
        for test_data_rec in test_data :
            #print len(test_data)
            result.append(self.get_prediction_result(test_data_rec))
        return result


    def display_result(self, results):
        for result, acc in results:
            if result == Constants.ATTACK_LABEL :
                print "Attack data.. \t PredictionProb: ", acc
            elif result == Constants.NO_ATTACK_LABEL :
                print "Normal Data.. No attack. \t PredictionProb: ", acc
            else :
                print Constants.ERROR_CODE , "No Accuracy"




if __name__ == '__main__':
    print "Starting training"


    classifier=SVMClassifier()

    results = classifier.get_predictions(Constants.TEST_DATA_FILE_PATH+ Constants.TEST_DATA_FILE_NAME)
    classifier.display_result(results)


    '''
    while True:
        testFileName = raw_input("Provide test filename with path or press ctrl+c to exit: ")
        actualPredictionFileName= raw_input("Provide test filename with actual result values with path :")
        testResults= classifier.get_prediction_result(testFileName)
        classifier.display_result(testResults)

    '''