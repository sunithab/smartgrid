clc
clear

LabelFontSize=16;

%%%%%%%%%%%%%%%%% boxnzcon%%%%%%%%%%%%%%%%%%%%%%%%%
xx=[ 9, 14 30 118 300];
load('Bus300.mat');
for i=1:cols
    for j=1:5
        t(i,j)=NaN;
    end
end       
for i=1:cols
    t(i,1)=rows-zerocounter(newF(:,i));
end
load('118bus.mat');
for i=1:cols
    t(i,2)=rows-zerocounter(newF(:,i));
end;

load('30bus.mat');
for i=1:cols
    t(i,3)=rows-zerocounter(newF(:,i));
end

load('14bus.mat');
for i=1:cols
    t(i,4)=rows-zerocounter(newF(:,i));
end

load('9bus.mat');
for i=1:cols
    t(i,5)=rows-zerocounter(newF(:,i));
end
t1=t(:,1)/1122;
t2=t(:,2)/490;
t3=t(:,3)/112;
t4=t(:,4)/54;
t5=t(:,5)/27;
boxplot([t1 t2 t3 t4 t5],'whisker',10);
%figure
set(gca,'XTickLabel',  {'300-bus'; '118-bus'; '30-bus';'14-bus';'9-bus'} );
set(gca,'fontsize',LabelFontSize);
xlabel('IEEE test systems','fontsize', LabelFontSize);
ylabel('Percentage of  meters to compromise','fontsize', LabelFontSize);
print -depsc2 -tiff figures/boxnzcon.eps
%%%%%%%%%%%%%%%%% boxtime%%%%%%%%%%%%%%%%%%%%%%%%%
xx=[ 9, 14 30 118 300];
load('Bus300.mat');
for i=1:cols
    for j=1:5
        timebox(i,j)=NaN;
        nzbox(i,j)=NaN;
    end
end
for i=1:cols
  timebox(i,1)=timeMP1(i); 
  nzbox(i,1)=nonzerosMP1(i);
end

load('118bus.mat');
for i=1:cols
  timebox(i,2)=timeMP1(i); 
  nzbox(i,2)=nonzerosMP1(i);
end
load('30bus.mat');
for i=1:cols
  timebox(i,3)=timeMP1(i); 
  nzbox(i,3)=nonzerosMP1(i);
end
load('14bus.mat');
for i=1:cols
  timebox(i,4)=timeMP1(i); 
  nzbox(i,4)=nonzerosMP1(i);
end
load('9bus.mat');
for i=1:cols
  timebox(i,5)=timeMP1(i); 
  nzbox(i,5)=nonzerosMP1(i);
end
figure
t1=timebox(:,1);
t2=timebox(:,2);
t3=timebox(:,3);
t4=timebox(:,4);
t5=timebox(:,5);
boxplot([t1 t2 t3 t4 t5],'whisker',10);
set(gca,'XTickLabel', {'300-bus';'118-bus'; '30-bus';'14-bus';'9-bus'} );
set(gca,'fontsize',LabelFontSize);
xlabel('IEEE test systems','fontsize', LabelFontSize);
ylabel('time(seconds)','fontsize', LabelFontSize);
print -depsc2 -tiff figures/boxtime.eps
%%%%%%%%%%%%%%%%%boxnz%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t1=nzbox(:,1);
t2=nzbox(:,2);
t3=nzbox(:,3);
t4=nzbox(:,4);
t5=nzbox(:,5);
figure
boxplot([t1 t2 t3 t4 t5],'whisker',10);
set(gca,'XTickLabel',  {'300-bus'; '118-bus'; '30-bus';'14-bus';'9-bus'} );
set(gca,'fontsize',LabelFontSize);
xlabel('IEEE test systems','fontsize', LabelFontSize);
ylabel('Number of meters to compromise','fontsize', LabelFontSize);
print -depsc2 -tiff figures/boxnz.eps
%%%%%%%%%%%%%%%%%300box10varcon%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('300box10var.mat');
t1=nonzerosMPboxcon(:,1);
t2=nonzerosMPboxcon(:,2);
t3=nonzerosMPboxcon(:,3);
t4=nonzerosMPboxcon(:,4);
t5=nonzerosMPboxcon(:,5);
t6=nonzerosMPboxcon(:,6);
t7=nonzerosMPboxcon(:,7);
t8=nonzerosMPboxcon(:,8);
t9=nonzerosMPboxcon(:,9);
t10=nonzerosMPboxcon(:,10);
figure
set(gca,'fontsize',LabelFontSize);
boxplot([t1 t2 t3 t4 t5 t6 t7 t8 t9 t10],'whisker',10);
xlabel('Number of target state variables','fontsize', LabelFontSize);
ylabel('Number of meters to compromise','fontsize', LabelFontSize);
print -depsc2 -tiff figures/300box10varcon.eps
%%%%%%%%%%%%%%%%%118box10varcon%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('118box10var.mat');
t1=nonzerosMPboxcon(:,1);
t2=nonzerosMPboxcon(:,2);
t3=nonzerosMPboxcon(:,3);
t4=nonzerosMPboxcon(:,4);
t5=nonzerosMPboxcon(:,5);
t6=nonzerosMPboxcon(:,6);
t7=nonzerosMPboxcon(:,7);
t8=nonzerosMPboxcon(:,8);
t9=nonzerosMPboxcon(:,9);
t10=nonzerosMPboxcon(:,10);
figure
set(gca,'fontsize',LabelFontSize);
boxplot([t1 t2 t3 t4 t5 t6 t7 t8 t9 t10],'whisker',10);
xlabel('Number of target state variables','fontsize', LabelFontSize);
ylabel('Number of meters to compromise','fontsize', LabelFontSize);
print -depsc2 -tiff figures/118box10varcon.eps
%%%%%%%%%%%%%%%%%300box10var%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('300bus10varmp.mat');
t1=nonzerosMPbox(:,1);
t2=nonzerosMPbox(:,2);
t3=nonzerosMPbox(:,3);
t4=nonzerosMPbox(:,4);
t5=nonzerosMPbox(:,5);
t6=nonzerosMPbox(:,6);
t7=nonzerosMPbox(:,7);
t8=nonzerosMPbox(:,8);
t9=nonzerosMPbox(:,9);
t10=nonzerosMPbox(:,10);
figure
set(gca,'fontsize',LabelFontSize);
boxplot([t1 t2 t3 t4 t5 t6 t7 t8 t9 t10],'whisker',10);
xlabel('Number of target state variables','fontsize', LabelFontSize);
ylabel('Number of meters to compromise','fontsize', LabelFontSize);
print -depsc2 -tiff figures/300box10var.eps
%%%%%%%%%%%%%%%%%118box10var%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('118bus10varmp.mat');
t1=nonzerosMPbox(:,1);
t2=nonzerosMPbox(:,2);
t3=nonzerosMPbox(:,3);
t4=nonzerosMPbox(:,4);
t5=nonzerosMPbox(:,5);
t6=nonzerosMPbox(:,6);
t7=nonzerosMPbox(:,7);
t8=nonzerosMPbox(:,8);
t9=nonzerosMPbox(:,9);
t10=nonzerosMPbox(:,10);
figure
set(gca,'fontsize',LabelFontSize);
boxplot([t1 t2 t3 t4 t5 t6 t7 t8 t9 t10],'whisker',10);
xlabel('Number of target state variables','fontsize', LabelFontSize);
ylabel('Number of meters to compromise','fontsize', LabelFontSize);
print -depsc2 -tiff figures/118box10var.eps
%%%%%%%%%%%%%%%%%300box10vartime%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('300bus10varmp.mat');
t1=timeMPbox(:,1);
t2=timeMPbox(:,2);
t3=timeMPbox(:,3);
t4=timeMPbox(:,4);
t5=timeMPbox(:,5);
t6=timeMPbox(:,6);
t7=timeMPbox(:,7);
t8=timeMPbox(:,8);
t9=timeMPbox(:,9);
t10=timeMPbox(:,10);
figure
set(gca,'fontsize',LabelFontSize);
boxplot([t1 t2 t3 t4 t5 t6 t7 t8 t9 t10],'whisker',10);
xlabel('Number of target state variables','fontsize', LabelFontSize);
ylabel('time(seconds)','fontsize', LabelFontSize);
print -depsc2 -tiff figures/300box10vartime.eps
%%%%%%%%%%%%%%%%%118box10vartime%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('118bus10varmp.mat');
t1=timeMPbox(:,1);
t2=timeMPbox(:,2);
t3=timeMPbox(:,3);
t4=timeMPbox(:,4);
t5=timeMPbox(:,5);
t6=timeMPbox(:,6);
t7=timeMPbox(:,7);
t8=timeMPbox(:,8);
t9=timeMPbox(:,9);
t10=timeMPbox(:,10);
figure
set(gca,'fontsize',LabelFontSize);
boxplot([t1 t2 t3 t4 t5 t6 t7 t8 t9 t10],'whisker',10);
xlabel('Number of target state variables','fontsize', LabelFontSize);
ylabel('time(seconds)','fontsize', LabelFontSize);
print -depsc2 -tiff figures/118box10vartime.eps