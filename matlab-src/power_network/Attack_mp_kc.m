function [attackvector,nz]=Attack_mp_kc(H,k,cc)

[m,n]=size(H);
attackvector=zeros(m,1);
I=eye(m,m);
temp=H;
b=zeros(m,1);
offset=0;
for i=1:length(cc)
   if(cc(i)~=0)
        b=b+cc(i)*temp(:,i-offset);
        temp(:,i-offset)=[];
        offset=offset+1;
   end
end

H=temp;
P=H*inv(H'*H)*H'-I;
y=P*b;
[x,tau]=Matching_Pursuit(P,y);
%[x,tau]=Matching_Pursuit_k(P,y,k);
nz=length(tau);
for i=1:length(tau)
           attackvector(tau(i))=x(i);
end