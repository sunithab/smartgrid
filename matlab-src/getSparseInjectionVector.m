function [c,unsafeColIndxs]= getSparseInjectionVector(H,numUnsafeCols) 
    [m,n]=size(H);
    rng('shuffle');
    %Attacker needs to get access to atleast m-n+1 meters for the attack to
    %be undetected by the StateEstimation
    allColIndxs=1:n;  
    if numUnsafeCols > n
        numUnsafeCols =n;
    end
    
    c=normrnd(0,0.1,[numUnsafeCols 1]);
    c=[c ; zeros(n-numUnsafeCols, 1)];
    c=c(randperm(length(c)));
    unsafeColIndxs=allColIndxs(c~=0);
end 
