function [p,G,B]=AdmittanceMatrixGenerator(casenumber)
%%%%%%%%%%%%%%%%%%%%%%%to be expanded later%%%%%%%%%%%%%%
[IEEE9_T, IEEE14_T, IEEE30_T, IEEE118_T] = Basic_IEEE_BranchData;
  if(casenumber==9)
     BranchMatrix=IEEE9_T;
  end
  if(casenumber==14)
     BranchMatrix=IEEE14_T;
  end
  if(casenumber==30)
     BranchMatrix=IEEE30_T;
  end
  if(casenumber==118)
     BranchMatrix=IEEE118_T;
  end
%%%%%%%%%%%%%%%%%%%%%%%to be expanded later%%%%%%%%%%%%%%
  [p,n]=size(BranchMatrix);
  m=casenumber;
  G=zeros(m,m);
  B=zeros(m,m);
  for i=1:p
      r=BranchMatrix(i,3);
      x=BranchMatrix(i,4);
      row1=BranchMatrix(i,1);
      col1=BranchMatrix(i,2);
      G(row1,col1)=-(r/((r*r)+(x*x)));
      B(row1,col1)=x/((r*r)+(x*x));
      G(col1,row1)=-(r/((r*r)+(x*x)));
      B(col1,row1)=x/((r*r)+(x*x));
  end
  for i=1:m
      sum1=0;
      sum2=0;
      for j=1:m
         sum1=sum1+G(i,j);
         sum2=sum2+B(i,j);
      end
      G(i,i)=-sum1;
      B(i,i)=-sum2;
  end
