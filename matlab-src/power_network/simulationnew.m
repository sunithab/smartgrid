 clc
clear

%% Start Simulation
load('IEEE300DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
newF=newF(:,2:cols);
load('XData300.mat');
 x=savedata';
 x(1)=[];
[rows,cols]=size(newF);
k=0;
 y=newF*x;
oldy=y;
fprintf('\nThe matrix H is a %d-by-%d matrix\n',rows,cols);

for i=1:cols
    c=zeros(cols,1);
    c(i)=randint(1,1,100)+1;
    attackvector=newF*c;
    nonezeros(i)=rows-zerocounter(attackvector);
end
nonezeros


avgnz=zeros(1,10);
for kk=1:100
   temp=randperm(cols);
for i=1:10
     c=zeros(cols,1);
     for j=1:i
        c(temp(j))=randint(1,1,100)+1;
     end
    attackvector=newF*c;
    avgnz(i)=avgnz(i)+rows-zerocounter(attackvector);
end
end
avgnz/100





 

