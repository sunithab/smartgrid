delimiter_in=','
data_path = fullfile( cd(cd('..')), 'data/lasso-iid/case_57/120');
train_data_file_name = fullfile(data_path, 'TrainData_NormalData_IID_case_57_attack_120.txt');
train_label_file_name =  fullfile(data_path, 'TrainLabel_NormalData_IID_case_57_attack_120.txt');

X1 = importdata(train_data_file_name,delimiter_in);
X=X1;%log(X1);
Y = importdata(train_label_file_name,delimiter_in);
c=zeros(size(X,1));
attackedIndexes= (Y==1) ;
attackedData=X(Y==1);

normalData=X(Y==0);
step=137
bins=1:step;
min_val=zeros(step,2);
max_val=zeros(step,2);
for i = 1:step
    tempX=X(2:137:end1)
    tempY=Y(2:137:end1)
    min_val(i)= 
end
    

scatter(X(attackedIndexes),X(attackedIndexes), [], 'r', 'x')
%xlim([175,200])
%ylim([175,200])
title('Normal & attacked measurements of IEEE 57-bus test system')
ylabel('z_i');
xlabel('z_i');
hold on
scatter(X(Y==0),X(Y==0), [], 'k')
hold on
legend('Attacked Meters','Unattacked Meters','Location','northwest'); 
%{
histfit(X)

mn=mean(X)
sd=std(X)
outlierUppBound=mn+2*sd
outlierLowBound=mn-2*sd 
temp= X(X<outlierUppBound) ; 
tempY=Y(X<outlierUppBound);
X1=temp(temp>outlierLowBound);
Y1=tempY(temp>outlierLowBound);

map = brewermap(2,'Set1'); 

h_a=histfit(X(attackedIndexes), 15,'kernel')
h_a(1).FaceColor = map(1,:);
hold on
h_u=histfit(X(Y==0),15,'kernel')
h_u(1).FaceColor =map(2,:)
h_u(2).Color = [.2 .2 .2];


f = figure;
p = uipanel('Parent',f,'BorderType','none');
p.Title = 'Plot of (z_i, z_i) showing the distribution of measurements in space';
p.TitlePosition = 'centertop';
p.FontSize = 12;
p.FontWeight = 'bold';

subplot(1,2,1,'Parent',p) 
scatter(1:137,A(:,:), [], 'r', 'x')
hold on
scatter(,Z(:,:), [], 'k')

xlabel('Meter i');
ylabel('Measurement of Meter i , z_i');

subplot(1,2,2,'Parent',p) 
scatter(1:137,log(Z(:,:)), [], 'k')
hold on
scatter(1:137,log(A(:,:)), [], 'r', 'x')
xlabel('Meter i');
ylabel('Logarithm of Measurement of Meter i , log(z_i)');

legend('Malacious Measurements','Secure Measurements','Location','northeast');


%}