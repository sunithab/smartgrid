function [attackvector]=Attack_BP_kc(H,k,cc)
[m,n]=size(H);
I=eye(m,m);
temp=H;
b=zeros(m,1);
offset=0;
for i=1:length(cc)
   if(cc(i)~=0)
        b=b+cc(i)*temp(:,i-offset);
        temp(:,i-offset)=[];
        offset=offset+1;
   end
end
H=temp;
P=H*inv(H'*H)*H'-I;
y=P*b;
[m,n]=size(P);
%Theta=randn(m,m);
%P=Theta*P;
%y=Theta*y;
%% Attacker Detection
% QR decomposition
[Q R]=qr(P);
newQ=Q(:,m-n+1:end);
ydata=Q'*y;
ydata=ydata(m-n+1:end);
%projection
% PH=P*inv(P'*P)*P';
% NPH=eye(m)-PH;
% YP=NPH*y;
% [i_list, alpha_list, error, exit_flag]=gbp_safe(NPH,YP');
[i_list, alpha_list, error, exit_flag]=gbp_safe(P',y');
DetAttacker=[];
for i=1:length(i_list)
    if (abs(alpha_list(i))>1e-3) 
        DetAttacker=[DetAttacker i_list(i)];
    end
end
DetAttacker=sort(DetAttacker);
fprintf('Generate Attackers : ');
for i=1:length(DetAttacker)
    fprintf('%d  ',DetAttacker(i));
end
fprintf('\n');
c=DetAttacker;
 length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       attackvector=zeros(m,1);
       length_c=length(c);
       I=eye(size(pinv(A)*A));
       z=10*ones(max(size(I)),1);
       x=pinv(A,1e-5)*y+(I-pinv(A,1e-5)*A)*z; 
       for i=1:length_c
           attackvector(c(i))=x(i);
       end
toc