function [coefs, dims] = pca_params(training_set)

    var_frac=0.99;
    train_out=training_set; % save original data
    mn = mean(train_out);
    train_out = bsxfun(@minus,train_out,mn); % substract mean
    [coefs,scores,variances] = princomp(train_out,'econ'); % PCA
    pervar = cumsum(variances) / sum(variances);
    dims = max(find(pervar < var_frac)); % var_frac - e.g. 0.99 - fraction of variance explained
end