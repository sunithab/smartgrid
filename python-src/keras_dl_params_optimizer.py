from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV
import numpy
import Constants as cs
import ClassifierUtils as cu
import logging

METRIC = 'accuracy'
# Function to create model, required for KerasClassifier
def create_model(optimizer='rmsprop', init='glorot_uniform', layer1=10, layer2=10, layer3=0):
    # create model
    model = Sequential()
    model.add(Dense(layer1, input_dim=1, init=init, activation='relu'))
    if layer2 > 0:
        model.add(Dense(layer2, init=init, activation='relu'))
    if layer3 > 0:
        model.add(Dense(layer3, init=init, activation='relu'))
    model.add(Dense(1, init=init, activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[METRIC])
    return model


def get_optimal_params_gridSearch(X,Y):
    # fix random seed for reproducibility
    seed = 7
    numpy.random.seed(seed)

    # create model
    model = KerasClassifier(build_fn=create_model, verbose=0)

    # grid search epochs, batch size and optimizer
    optimizers = ['rmsprop']#, 'adam']
    init = ['glorot_uniform']#, 'normal', 'uniform']
    epochs = [100]
    batches = [5]
    layer1 = [3] #[300,350,400]
    layer2 = [4] #[ 150,200, 250]
    layer3 =[0] #[0, 150, 200, 250]
    
    # Initialize parameters for grid search
    param_grid = dict(optimizer=optimizers, nb_epoch=epochs, batch_size=batches, init=init, layer1=layer1, layer2=layer2,  layer3=layer3)
    grid = GridSearchCV(estimator=model, param_grid=param_grid, verbose=3, n_jobs=-1, cv=10, scoring=METRIC)
    grid_result = grid.fit(X, Y)

    # summarize results
    stmt = "Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_)
    logging.info(stmt)
    print stmt
    best_model=grid_result.best_estimator_.model
    best_model.save(cs.TRAINING_DATA_FILES_PATH + "bestmodel_" + cs.TRAINING_DATA_FILE_NAME[0:-4]+'_test.h5')
    # creates a HDF5 file 'my_model.h5'

    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']

    for mean, stdev, param in zip(means, stds, params):
        stmt = "\n%f (%f) with: %r" % (mean, stdev, param)
        logging.info(stmt)
    return grid_result.best_score_, grid_result.best_params_


def filter_outliers(X, Y):
    import numpy as np
    X1 = []
    Y1 = []
    mn = np.mean(X)
    sd = np.std(X)
    outlierUppBound = mn + 2 * sd
    outlierLowBound = mn - 2 * sd
    for indx in range(np.size(X)):
        if (X[indx] > outlierLowBound and X[indx] < outlierUppBound):
            X1.append(X[indx])
            Y1.append(Y[indx])

    newX = np.array(X1, dtype=float)
    newY = np.array(Y1, dtype=int)
    # print newX, newY , np.size(X) , np.size(Y)
    return newX, newY

def load_dataset(removeOutliers=False) :
    # Load dataset
    X = cu.get_file_data(cs.TRAINING_DATA_FILES_PATH + cs.TRAINING_DATA_FILE_NAME,
                         delimiter=",", dtype=float)
    Y = cu.get_file_data(cs.TRAINING_DATA_FILES_PATH + cs.TRAINING_LABEL_FILE_NAME,
                         delimiter=",", dtype=int)
    if removeOutliers :
      X, Y= filter_outliers(X, Y)
    return X, Y

def init_logger():
    fileName = cs.TRAINING_DATA_FILES_PATH + "Output_" + cs.TRAINING_DATA_FILE_NAME[0:-4] + ".txt"
    logging.basicConfig(filename=fileName, level=logging.INFO)
    logging.info("---------------------------------------------------------------------------------" \
          "----------------------------------------------------------------\n")

def train_model(caseNum, numOfAttackedMeters):
    import  time
    cs.updateFileNames(caseNum, numOfAttackedMeters)
    init_logger()

    startTime = time.time()

    X,Y = load_dataset()
    #X, Y = load_dataset(removeOutliers=True)
    best_score, best_params=get_optimal_params_gridSearch(X,Y)

    stmt = "Time taken (sec): " , time.time()-startTime
    logging.info(stmt)

    write_to_file(caseNum,numOfAttackedMeters, best_score, best_params)

    return

def write_to_file(caseNum, num_of_attacked_meters, best_score, best_params ):
    import os
    os.chdir(cs.TRAINING_DATA_FILES_PATH)
    os.chdir('..')
    fileName=os.path.join(os.path.abspath(os.curdir), "best_scores_report.txt")
    print fileName
    if os.path.isfile(fileName) :
        fobj = open(fileName, "a")
    else :
        fobj = open(fileName, "w")
        fobj.write("caseNum\tMetersAttacked\tBest Accuracy\tBest Params\t")

    stmt="\n"+str(caseNum)+"\t"+str(num_of_attacked_meters)+"\t"+str(best_score)+"\t" +str(best_params)
        #caseNum, "\t",num_of_attacked_meters, "\t", best_score, "\t", best_params
    fobj.write(stmt)


    return

if __name__ == "__main__":
    import sys
    params= sys.argv[1:]
    print params
    caseNum=params[0]
    numOfAttackedMeters=params[1]
    train_model(caseNum, numOfAttackedMeters)