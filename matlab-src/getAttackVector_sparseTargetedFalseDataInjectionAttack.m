function [a]= getAttackVector_sparseTargetedFalseDataInjectionAttack(H, numOfAttackedMeters) 
    %generate attack vector a and add that to the normal vector z

    %%note!!! There are different ways to generate attack vector a, which
    %%depends on which kind of attack you want to launch.
    %refer paper: 1) http://arxiv.org/pdf/1502.04254.pdf
    %             2) http://dl.acm.org/citation.cfm?id=1653666
    %              3) http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6032057
    % for details.
        
    %%m-n+1 values need of H need to be compromosed to generate an attack
    %%vector successfully[2]. The attack generation method[1] (section III, method 1) 
    %%used here generates such attacks using sparseness of injection vector 'c'. 
    %%so here we try to vary sparseness of 'c' and try to generate such attack vectors. 
    [m,n]=size(H);
%    assert(numOfAttackedMeters<n);

    %Attacked data Based on paper-1 above
    
    % make z is not linear dependent with each other
    [c, attackedMeters]=getSparseInjectionVector(H, numOfAttackedMeters);
    [B_s,y] = getProjectionMatrices(H,c,attackedMeters);
    a = getAttackVectorCandidate(B_s, y, H, c, numOfAttackedMeters) ;
end