#Keras Details: https://github.com/fchollet/keras/blob/master/README.md

import numpy as np
import Constants as cs
import ClassifierUtils as cu

from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold


# baseline model
def create_baseline():
    num_of_features=1
    # create model
    model = Sequential()
    model.add(Dense(60, input_dim=num_of_features, init='normal', activation='relu'))
    model.add(Dense(1, init='normal', activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def get_model_metrics() :
    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)

    # Load dataset
    X = cu.get_file_data(cs.TRAINING_DATA_FILES_PATH + cs.TRAINING_DATA_FILE_NAME,
                         delimiter=",", dtype=float)
    Y = cu.get_file_data(cs.TRAINING_DATA_FILES_PATH + cs.TRAINING_LABEL_FILE_NAME,
                         delimiter=",", dtype=int)

    # evaluate model with standardized dataset
    estimator = KerasClassifier(build_fn=create_baseline, nb_epoch=100, batch_size=5, verbose=0)
    kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=seed)
    results = cross_val_score(estimator, X, Y, cv=kfold)
    print("Results: %.2f%% (%.2f%%)" % (results.mean() * 100, results.std() * 100))

    return

def get_all_label_metrics():
    from sklearn.model_selection import KFold
    from sklearn.metrics import classification_report, precision_recall_fscore_support
    from keras import optimizers

    # fix random seed for reproducibility
    seed = 7
    np.random.seed(seed)

    # Load dataset
    X = cu.get_file_data(cs.TRAINING_DATA_FILES_PATH + cs.TRAINING_DATA_FILE_NAME,
                         delimiter=",", dtype=float)
    Y = cu.get_file_data(cs.TRAINING_DATA_FILES_PATH + cs.TRAINING_LABEL_FILE_NAME,
                         delimiter=",", dtype=int)

    labels = list(set(Y))
    avg_p = [0, 0]
    avg_r = [0, 0]
    n_splits = 3

    #clf = KerasClassifier(build_fn=create_baseline, nb_epoch=100, batch_size=5, verbose=0)
    #Load classifier from h5 file
    clf=load_model()
    for layer in clf.model.layers:
        layer.trainable = True
    clf.model.compile(loss='binary_crossentropy',
                  optimizer=optimizers.SGD(lr=1e-4, momentum=0.9),
                  metrics=['accuracy'])
    kf = KFold(n_splits=n_splits)
    #kf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=seed)
    for train, test in kf.split(X):
        X_train, X_test, y_train, y_test = X[train], X[test], Y[train], Y[test]
        clf.fit(X_train, y_train)
        y_pred_prob = clf.predict(X_test)
        y_pred = [float(round(x)) for x in y_pred_prob]
        p, r, f1, s = precision_recall_fscore_support(y_test, y_pred, labels=labels, average=None)
        avg_p = avg_p + p;
        avg_r = avg_r + r;

    print "Avg Recall    Class",labels," : ", avg_r / n_splits

    print "Avg Precision Class",labels," : ", avg_p / n_splits

def load_model() :
    from keras.models import load_model
    fileName = cs.TRAINING_DATA_FILES_PATH + "bestmodel_" + cs.TRAINING_DATA_FILE_NAME[0:-4]+'_test.h5'
    clf=load_model(fileName)

    return clf

if __name__ == "__main__":
    cs.updateFileNames(caseNum='case_118', numOfAttackedMeters='54')
    #get_model_metrics();
    get_all_label_metrics();