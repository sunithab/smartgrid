function [rest v]=diffi(a, b)

%a=[1 1 1 0 1 3 0 1 0 5 10 0 0  1 1 1]
%b=[0 1 1 0 1 3 0 0 1 2 10 0 0 2 0 0]
l=length(a);
for i=1:l
    if (abs(a)<1e-5)
        a(i)=0;
    end
    if (abs(b)<1e-5)
        b(i)=0;
    end
end
pos_a=find(abs(a)~=0);
pos_b=find(abs(b)~=0);

l=length(pos_b);
commvector=[];
for i=1:l
    if length(find(pos_a==pos_b(i)))
        commvector=[commvector pos_b(i)];
    end
end
 
if length(commvector)==0
    rest=a;v=10;
    return
end
commNum=length(commvector);
commRate=a([commvector])./b([commvector]);

l=length(commRate);
freq=ones(1,l);

for i=1:l-1
    for j=i+1:l
        if abs(commRate(i)-commRate(j))<1e-4
            freq(i)=freq(i)+1;
        end
    end
end

[maxvalue maxpos]=max(freq);


v=length(pos_b)-commNum-maxvalue;
rest=a-b*commRate(maxpos(1));
