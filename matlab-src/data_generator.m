%Guassian Noise
caseNum=9
numOfSamples=1
numOfAttackedMeters=5;
isTestData=false
%generateAttackVector_anyCase(caseNum, numOfSamples, numOfAttackedMeters, isTestData);

%118 -> m:490 ; 30 -> m: 112; 57(mete) -> m:137; 9 -> m:27 

m=27; %TODO Change m based on caseNum being used
n=caseNum-1;
rangeVals=unique(round(linspace(3,m,20))) ;
rangeVals=[rangeVals m-n+1];
for i=1:numel(rangeVals)
    numOfAttackedMeters=rangeVals(i);
    generateAttackVector_anyCase(caseNum, numOfSamples, numOfAttackedMeters, isTestData);
end
%{
num_of_iterations = 1;
for i=1:num_of_iterations 
    display(i)
    AttackVector_case14();
end  
%}