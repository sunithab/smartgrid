function [attackvector]=Attack_Stack_k(H,k)
flag=0;
[rows,cols]=size(H);
maxvector=H(:,1);
max=zerocounter(H(:,1));
% for i=2:cols
%     if(zerocounter(H(:,i))>=max)
%        maxvector=H(:,i);
%        max=zerocounter(H(:,i));
%     end
% end
stack=H;
while (~isempty(stack))
     h=stack(:,1);
     %%%%%%%%%%%%%%%%%%%
     if(zerocounter(h)>=rows-k)
            flag=1;
            break;
     end
     %%%%%%%%%%%%%%%%%%%
     stack(:,1)=[];
     [m, n]=size(stack);
     length=n;
     for i=1:length
         t=stack(:,i);
         if(norm(h-t)<1e-6)
            continue;
         end
        [newh,d]=diffi(h',t'); 
        newh=newh'; 
         if(d<=0) 
            if(max<zerocounter(newh))
                  max=zerocounter(newh);
                  maxvector=newh;
                  stack=horzcat(stack,newh);
                 % entercolumn=entercolumn+1 % count the number of entered columns
            end
         end
     end
end
if(flag==1)
   fprintf('\nStack K Bingo! The %d  attack vector is:\n',rows-k);
   attackvector=h;
else
  fprintf('\ncan not find a %d attack vector!, the best attack vector has %d zero elements\n',rows-k,max);
  attackvector=maxvector;
end

