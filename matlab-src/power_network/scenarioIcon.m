clc
clear

%% Start Simulation
load('IEEE300DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
newF=newF(:,2:cols);
[rows,cols]=size(newF);
fprintf('\nThe matrix H is a %d-by-%d matrix\n',rows,cols);
k=350;
samplenum=6;
target=zeros(samplenum,1);
a=zeros(k,samplenum);
c=eye(cols, cols);
flag=0;
for i=1:samplenum
   temp=randperm(rows);
   for j=1:k
       a(j,i)=temp(j);
   end
   for kk=1:cols  %pick every c
       flag=0;
       tic
        a1=newF*c(:,kk)*1000; % get the corresponding attack vector
        for ii=1:rows  %check if there is non-zero elements whose postion is not in a
             if(abs(a1(ii))>10e-5)
                 if(belongto(a(:,i),ii)==0)
                      flag=1;
                      break;
                 end
             end
        end
        if(flag==0)
           target(i)= target(i)+1;
        end
       timercon(kk)=toc;
   end
end
target

bar(target);
set(gca,'XTickLabel', {'set1'; 'set2'; 'set3'; 'set4';'set5';'set6'} );
ylabel('Number of target state variables');
grid on

%samplecon













 

