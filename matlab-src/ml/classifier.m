function [model, feat_trans] = classifier(train_set, train_label, model_name, feature_trans_name)
    classes=unique(train_label);
    numClasses=length(classes);
    feat_trans = cell(numClasses, 2);
    num_features = size(train_set,2);
    num_selected_features = 200;
    k=1;

    %Vectorized statement that binarizes Group
        %where 1 is the current class and 0 is all other classes
        G1vAll=train_label;
        train_out = train_set;
        switch lower(feature_trans_name)
            case 'pca'
                [coefs, dims] = pca_params(train_out);
                feat_trans{k,1}=coefs;
                feat_trans{k,2}=dims;
                train_out=pca_transform(coefs,dims,train_out);

            case 'mrmr'
                ranking = mRMR(train_out, train_label, num_features);
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'relieff'
                [ranking, w] = reliefF( train_out, train_label, 9);
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'mutinffs'
                [ ranking , w] = mutInfFS( train_out, train_label, num_features );
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'fsv'
                [ ranking , w] = fsvFS( train_out, train_label, num_features );
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'laplacian'
                W = dist(train_out');
                W = -W./max(max(W)); % it's a similarity
                [lscores] = LaplacianScore(train_out, W);
                [junk, ranking] = sort(-lscores);
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'mcfs'
                % MCFS: Unsupervised Feature Selection for Multi-Cluster Data
                options = [];
                options.k = 5; %For unsupervised feature selection, you should tune
                %this parameter k, the default k is 5.
                options.nUseEigenfunction = 4;  %You should tune this parameter.
                [FeaIndex,~] = MCFS_p(train_out,num_features,options);
                ranking = FeaIndex{1};
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'rfe'
                ranking = spider_wrapper(train_out,train_label,num_features,lower(selection_method));
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'l0'
                ranking = spider_wrapper(train_out,train_label,num_features,lower(selection_method));
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'fisher'
                ranking = spider_wrapper(train_out,train_label,num_features,lower(selection_method));
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'inffs'
                % Infinite Feature Selection 2015 updated 2016
                alpha = 0.5;    % default, it should be cross-validated.
                sup = 1;        % Supervised or Not
                [ranking, w] = infFS( train_out , train_label, alpha , sup , 0 );    
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'ecfs'
                % Features Selection via Eigenvector Centrality 2016
                alpha = 0.5; % default, it should be cross-validated.
                ranking = ECFS( train_out, train_label, alpha )  ;
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'udfs'
                % Regularized Discriminative Feature Selection for Unsupervised Learning
                nClass = 2;
                ranking = UDFS(train_out , nClass ); 
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'cfs'
                % BASELINE - Sort features according to pairwise correlations
                ranking = cfs(train_out);    
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features; 
                train_out=train_out(:,ranking(1:num_selected_features));

            case 'llcfs'   
                % Feature Selection and Kernel Learning for Local Learning-Based Clustering
                ranking = llcfs( train_out );
                feat_trans{k,1}=ranking;
                feat_trans{k,2}=num_selected_features;
                train_out=train_out(:,ranking(1:num_selected_features));

            otherwise
                %disp('Unknown method.')   ; 


        end
        switch upper(model_name)
            case 'SVM'
                model = fitcsvm(train_out, G1vAll,'Standardize',true);%,'KernelFunction','linear');
                %model = svmtrain(train_out,G1vAll);%, 'kernel_function', 'rbf');
                
            case 'KNN'
                model=fitcknn(train_out, G1vAll,'NumNeighbors',9,'Standardize',1,'BreakTies', 'nearest');
            case 'NB'
                model=fitcnb(train_out, G1vAll);
            case 'BOOST'
                rng default % For reproducibility
                temp = 500;
                model = fitensemble(train_out, G1vAll,'RUSBoost',temp,'Tree');
            case 'RF'
                rng(1); % For reproducibility
                model = TreeBagger(50,train_out,G1vAll,'OOBPrediction','On', 'Method','classification');
            case 'LOGIS_REG' 
                 model =logitBin(train_out,G1vAll, 0.1);
        end

    
end