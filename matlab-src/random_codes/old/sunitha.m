
%IEEE Case14 DC
load('IEEE14DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
H=newF(:,2:cols);
[m,n]=size(H);

%load case14 bus system
mpc = loadcase('case14');
define_constants;

%how many data you want to generate.
length =500;
%normal data
Z=zeros(m,length);
%normal data with noise
Z1=zeros(m,length);

%generate a series of normal vector z
mpopt=mpoption('out.all',0);
for i=1:length
    results=rundcpf(mpc,mpopt);
    x=results.bus(:,9);
    x=x(2:14,1);
    z=H*x;
    Z(:,i)=z;
end
disp(Z)
dlmwrite('NormalData.txt',Z)

%generate a series of normal vector z with Noise
mpopt=mpoption('out.all',0);
for i=1:length
    e=normrnd(0,0.1,[m 1]);
    results=rundcpf(mpc,mpopt);
    x=results.bus(:,9);
    x=x(2:14,1);
    z=H*x+e;
    Z1(:,i)=z;
end
disp(Z1)
dlmwrite('NormalDataWithNoise.txt',Z1)

%generate attack vector a and add that to the normal vector z

%%note!!! There are different ways to generate attack vector a, which
%%depends on which kind of attack you want to launch.
%refer paper: 1) http://arxiv.org/pdf/1502.04254.pdf
%             2) http://dl.acm.org/citation.cfm?id=1653666
%              3) http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6032057
% for details.

%attacked data
A1=zeros(m,length);

%Guassian Noise
for i=1:length
    e=normrnd(0,0.1,[m 1]);
    % make z is not linear dependent with each other
    c=rand(n,1)*i;
    a=H*c;
    a1=Z(:,i)+a+e;
    A1(:,i)=a1;
end
dlmwrite('AttackData_1.txt',Z1)
% Z1 is a matrix that contains a series of attacked data.