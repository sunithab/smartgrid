clear all;
lib_path = fullfile( cd(cd('..')), 'feature_selection_lib');
addpath(lib_path, fullfile(lib_path,'lib'), fullfile(lib_path,'methods'));

data_path = fullfile( cd(cd('../..')), 'data');
test_data_path= fullfile(data_path, 'test');
train_data_file_name = fullfile(data_path, 'training_data.txt');
train_label_file_name =  fullfile(data_path, 'training_label.txt');

test_data_file_name = fullfile(test_data_path, 'test_file.txt');
test_label_file_name = fullfile(test_data_path, 'test_result_label_file.txt');


precision = @(confusionMat) (diag(confusionMat)./sum(confusionMat,2));
recall = @(confusionMat) mean(diag(confusionMat)./sum(confusionMat,1)');
f1Scores = @(confusionMat) mean(2*(precision(confusionMat).*recall(confusionMat))./(precision(confusionMat)+recall(confusionMat)))

X = importdata(train_data_file_name);
Y = importdata(train_label_file_name);
T = importdata(test_data_file_name);
T_Y = importdata(test_label_file_name);

modelName = 'SVM'; %models{k} = svmtrain(train_out,G1vAll);
feature_trans_name = 'none' ;

[models, feature_trans] = classifier(X, Y, modelName, feature_trans_name);
P= predictor(T, models, modelName, feature_trans, feature_trans_name);
  

   
%Write output to file
output_data_path=fullfile(data_path, 'results');
test_res_file_name =  fullfile(output_data_path, strcat('result_',modelName,'_fea_',feature_trans_name,'.txt'));
dlmwrite(test_res_file_name ,P,'delimiter','\t');
disp(strcat('Output written at :  ' , test_res_file_name));

% updates the CP object with the current classification results
accuracy = sum(T_Y == P) / numel(T_Y)
cp = classperf(T_Y,P);
conf_martix = confusionmat(T_Y, P);
precision=  precision(conf_martix)
recall= recall(conf_martix)
f1=  f1Scores(conf_martix)