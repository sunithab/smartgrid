function [data_out] = pca_transform(coefs, dims, data_set)
    data_out = data_set*coefs(:,1:dims); % dims - keep this many dimensions
end