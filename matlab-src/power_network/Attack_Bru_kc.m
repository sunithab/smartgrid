function [attackvector]=Attack_Bru_kc(H,k,cc)
[m,n]=size(H);
I=eye(m,m);
flag=0;
temp=H;
b=zeros(m,1);
offset=0;
for i=1:length(cc)
   if(cc(i)~=0)
        b=b+cc(i)*temp(:,i-offset);
        temp(:,i-offset)=[];
        offset=offset+1;
   end
end
H=temp;
P=H*inv(H'*H)*H'-I;
y=P*b;
r=k;
c=zeros(1,r);
   for j=1:r
      c(j)=j;
 %     fprintf('%d ',c(j));  
   end
   %    fprintf('\n');
       length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       [a,b]=size(A);
      if(rank(A,10e-5)==rank(horzcat(A,y),10e-5))
             attackvector=zeros(m,1);
             length_c=length(c);
             I=eye(size(pinv(A)*A));
             z=10*ones(max(size(I)),1);
             x=pinv(A,1e-5)*y+(I-pinv(A,1e-5)*A)*z;
            for i=1:length_c
                  attackvector(c(i))=x(i);
            end
            flag=1;
            fprintf('\nBrute Force Kc Bingo! The %d  attack vector is:\n',m-k);
            return;
       end  
   i=r;
   while(i>0)
       if (c(i)<m-r+i)
           c(i)=c(i)+1;
           for j=i+1:r
               c(j)=c(j-1)+1;
           end
       for j=1:r
    %        fprintf('%d ',c(j));
       end
      %     fprintf('\n');
       length_c=length(c);
       A=P(:,c(1));
       if(length_c>=2)
          for ii=2: length_c
               A=horzcat(A,P(:,c(ii)));
          end
       end
       [a,b]=size(A);
       if(rank(A,10e-5)==rank(horzcat(A,y),10e-5))
             flag=1;
             break;
       end
        i=r;  
      else
        i=i-1;
       end
   end
if(flag==1)
    attackvector=zeros(m,1);
    length_c=length(c);
    I=eye(size(pinv(A)*A));
    z=10*ones(max(size(I)),1);
    x=pinv(A,1e-5)*y+(I-pinv(A,1e-5)*A)*z; 
    for i=1:length_c
          attackvector(c(i))=x(i);
    end
else
   attackvector=zeros(m,1); 
end
if(zerocounter(attackvector)==m)
   %  fprintf('\n%d attack vector does not exist !\n',m-k);
 else
    fprintf('\nBrute Force Kc Bingo! The %d  attack vector is:\n',m-k);
 end