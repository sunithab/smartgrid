function []= persistUniqueDataRowsToFile(caseNum,numOfAttackedMeters,attacktype, Z,Z1,A,A_with_noise,A_attackedMeters,isTestData)
    %This function first finds the unique values of Z and saves every
    %row in a separate line in the file, as a result, the file will
    %have n columns and m rows, if all rows are unique.
    
%TODO disp(Z)    
    dataPath = fullfile( cd(cd('..')), 'data');
    if isTestData 
        dataPath = fullfile( dataPath, 'test');
    end 
    dataPath = fullfile( dataPath, attacktype);  %eg : lasso , iid or any other    
    folderName=sprintf('case_%d',caseNum);
    mkdir(dataPath,folderName);
    dataPath = fullfile( dataPath, folderName);
    mkdir(dataPath,num2str(numOfAttackedMeters));
    dataPath = fullfile( dataPath, num2str(numOfAttackedMeters))
    trainNormalFormatSpec = 'Train%s_RowNormalData_IID_case_%d_attack_%d.txt';% from (z+Z1) and A
    trainNoisyFormatSpec = 'Train%s_RowNoisy_Data_IID_case_%d_attack_%d.txt'; % from Z1 and A_with_noise
    attackFormatSpecMeters = 'RowAttackMeters_case_%d_%d.txt';   
    
    %%Generate Training data without noise
    newDim=size(Z,1);
    tempZ1 = horzcat(Z1, zeros(newDim, 1));
    tempZ = horzcat(Z, zeros(newDim, 1));
    tempA1 = horzcat(A, any(A_attackedMeters,2));
    train_normalData1=unique(round([tempZ; tempZ1; tempA1 ], 6), 'rows');        
    train_normalData=removeRepeatedElements(train_normalData1);

    train_data=train_normalData(:,1:end-1);
    train_labels= train_normalData(:,end);
    dlmwrite(fullfile(dataPath, sprintf(trainNormalFormatSpec, 'Data',caseNum, numOfAttackedMeters)),train_data);
    dlmwrite(fullfile(dataPath, sprintf(trainNormalFormatSpec, 'Label',caseNum, numOfAttackedMeters)),train_labels);
    disp('Generated attack data without noise');

    %Generate Training data with noise
    tempZ1 = horzcat(Z1, zeros(newDim, 1));
    tempZ = horzcat(Z, zeros(newDim, 1));
    tempA1 = horzcat(A_with_noise, any(A_attackedMeters,2));
    train_noiseData1=unique(round([tempZ; tempZ1; tempA1 ], 6), 'rows');        
    train_noiseData=removeRepeatedElements(train_noiseData1);

    train_data=train_noiseData(:,1:end-1);
    train_labels= train_noiseData(:,end);
    dlmwrite(fullfile(dataPath, sprintf(trainNoisyFormatSpec, 'Data',caseNum, numOfAttackedMeters)),train_data);
    dlmwrite(fullfile(dataPath, sprintf(trainNoisyFormatSpec, 'Label',caseNum, numOfAttackedMeters)),train_labels);
    disp('Generated attack data with noise');
    
    dlmwrite(fullfile(dataPath, sprintf(attackFormatSpecMeters, caseNum, numOfAttackedMeters)), A_attackedMeters);
    disp('Generated attacked meters');

end