function [B_s,y]= getProjectionMatrices(H,c,unsafeColIndxs) 
    [m,n]=size(H);

    allColIndxs=1:n;  
    safeColIndxs = sort(setdiff(allColIndxs,unsafeColIndxs));

    %Generate unsafe submatrices
    H_u = H(:, unsafeColIndxs);
    c_u = c(unsafeColIndxs,1);

    %Generate safe submatrices
    H_s = H(:, safeColIndxs);
    c_s = c(safeColIndxs,1);

    %Calculate other matrices
    b = H_u * c_u;
    temp= inv(H_s' * H_s);
    P_s = (H_s * temp) * H_s';
    B_s = P_s - eye(m);
    y = B_s * b;
end 
