function []= persistUniqueDataElementsToFile(caseNum,numOfAttackedMeters,attacktype, Z,Z1,A,A_with_noise,A_attackedMeters,isTestData)
    %This function first finds the unique values of Z and saves every
    %element in a separate row in the file, as a result, the file will
    %have one column and m X n rows, if all elements are unique.
    %This function saves every element in a separate row in the file,
    %as a result, the file will have one column and m X n rows
    dataPath = fullfile( cd(cd('..')), 'data');
    if isTestData 
        dataPath = fullfile( dataPath, 'test');
    end
    dataPath = fullfile( dataPath, attacktype);  %eg : lasso , iid or any other
    folderName=sprintf('case_%d',caseNum);
    mkdir(dataPath,folderName);
    dataPath = fullfile( dataPath, folderName);
    mkdir(dataPath,num2str(numOfAttackedMeters));
    dataPath = fullfile( dataPath, num2str(numOfAttackedMeters))
    trainNormalFormatSpec = 'Train%s_NormalData_IID_case_%d_attack_%d.txt';% from (z+Z1) and A1
    trainNoisyFormatSpec = 'Train%s_Noisy_Data_IID_case_%d_attack_%d.txt'; % from Z1 and A1_with_noise

    %%Generate Training data without noise
    newDim=numel(Z);
    tempZ1 = horzcat(reshape(Z1, [newDim, 1]), zeros(newDim, 1));
    tempZ = horzcat(reshape(Z, [newDim, 1]), zeros(newDim, 1));
    tempA1 = horzcat(reshape(A, [newDim, 1]), reshape(A_attackedMeters, [newDim, 1]));
    train_normalData=round([tempZ; tempZ1; tempA1 ], 6);
    train_normalData1=unique(train_normalData, 'rows');        
    train_normalData=removeRepeatedElements(train_normalData1);

    train_data=train_normalData(:,1:end-1);
    train_labels= train_normalData(:,end);
    dlmwrite(fullfile(dataPath, sprintf(trainNormalFormatSpec, 'Data',caseNum, numOfAttackedMeters)),train_data);
    dlmwrite(fullfile(dataPath, sprintf(trainNormalFormatSpec, 'Label',caseNum, numOfAttackedMeters)),train_labels);
    disp('Generated attack data without noise');

    %Generate Training data with noise
    tempA1_with_noise = horzcat(reshape(A_with_noise, [newDim, 1]), reshape(A_attackedMeters, [newDim, 1]));
    train_noiseData1=unique(round([tempZ; tempZ1; tempA1_with_noise ],6), 'rows');  
    train_noiseData=removeRepeatedElements(train_noiseData1);      


    train_data=train_noiseData(:,1:end-1);
    train_labels= train_noiseData(:,end);
    dlmwrite(fullfile(dataPath, sprintf(trainNoisyFormatSpec, 'Data',caseNum, numOfAttackedMeters)),train_data);
    dlmwrite(fullfile(dataPath, sprintf(trainNoisyFormatSpec, 'Label',caseNum, numOfAttackedMeters)),train_labels);
    disp('Generated attack data with noise');
    
end
