%%%%%%%%%%% 9busvstimeHeurBF %%%%%%%%%%%%%%%%%%%%%
x=[ 4          5         6         7        8         9          10];
y1=[0.0010    0.0002    0.0002    0.0002    0.0002    0.0002    0.0002];
y2=[0.0432    0.0293    0.0107    0.0100    0.0092    0.0073    0.0058];
semilogy(x,y1,'ko-')
hold on
semilogy(x,y2,'k+-')
hold off
xlabel('# of non-zero elements in the attack vector(IEEE9bus)');
ylabel('time(seconds)');
legend('Heuristic Algorithm', 'Brute-force Algorithm');
grid on
%%%%%%%%%%%%% 300busvstime %%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=10:20
    x(i-9)=i;
end
y=[21.7479   21.5327   21.5336    0.0361    0.0360    0.0356    0.0364    0.0365    0.0359  0.0363    0.0361];
figure
semilogy(x,y,'ko-')
xlabel('# of non-zero elements in the attack vector(IEEE300bus)');
ylabel('time(seconds)');
%legend('Heuristic Algorithm');
grid on
%%%%%%%%%%%%%%% 118busvstime %%%%%%%%%%%%%%%%%%%%%%%%%%
for i=10:20
    x(i-9)=i;
end
y=[0.0099    0.0122    0.0099    0.0088    0.0121    0.0086    0.0104    0.0094    0.0094 0.0098    0.0123];
figure
semilogy(x,y,'ko-')
xlabel('# of non-zero elements in the attack vector(IEEE118bus)');
ylabel('time(seconds)');
%legend('Heuristic Algorithm');
grid on
%%%%%%%%%%%%%%% 300busvstargestime %%%%%%%%%%%%%%%%%%%%%%%%%%
x=zeros(1,10);
for i=1:10
    x(i)=i;
end
y=[2.1492    1.9083    4.0161    4.8188    5.3437    6.3118    7.4362    8.1889    8.6826  10.4772];
figure
plot(x,y,'ko-')
xlabel('# of target state variables(IEEE300bus)');
ylabel('time(seconds)');
%legend('Matching Pursuit Algorithm');
grid on
%%%%%%%%%%%%%%% 300busvstargesnz %%%%%%%%%%%%%%%%%%%%%%%%%%
x=zeros(1,10);
for i=1:10
    x(i)=i;
end
y=[14        18        45        54        61        73       86         100       105   128];
figure
plot(x,y,'ko-')
xlabel('# of target state variables(IEEE300bus)');
ylabel('# of non-zero elements in the attack vector');
%legend('Matching Pursuit Algorithm');
grid on
%%%%%%%%%%%%%%%% 118busvstargestime %%%%%%%%%%%%%%%%%%%%%%%%%
x=zeros(1,10);
for i=1:10
    x(i)=i;
end
y=[0.3699    0.3394    0.7439    1.0257    0.8946    1.0664    1.3110    1.2939    1.2995   1.5926];
figure
plot(x,y,'ko-')
xlabel('# of target state variables(IEEE118bus)');
ylabel('time(seconds)');
%legend('Matching Pursuit Algorithm');
grid on
%%%%%%%%%%%%%%%  118busvstargesnz %%%%%%%%%%%%%%%%%%%%%%%%%%
x=zeros(1,10);
for i=1:10
    x(i)=i;
end
y=[14        18        45        54        61        73       86         100       105   128];
figure
plot(x,y,'ko-')
xlabel('# of target state variables(IEEE118bus)');
ylabel('# of non-zero elements in the attack vector');
%legend('Matching Pursuit Algorithm');
grid on
%%%%%%%%%%%%%%%%% 300indicetime%%%%%%%%%%%%%%%%%%%%%%%%%
load('Bus300.mat');
x=zeros(1,299);
for i=1:299
    x(i)=i+1;
end
y=timeMP1;
figure
plot(x,y)
xlabel('Indice of target state variables(IEEE300bus)');
ylabel('time(seconds)');
%legend('Matching Pursuit Algorithm');
grid on

%%%%%%%%%%%%%%% 300indicenz%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('Bus300.mat');
x=zeros(1,299);
for i=1:299
    x(i)=i+1;
end
y=nonzerosMP1;
figure
plot(x,y)
xlabel('Indice of target state variables(IEEE300bus)');
ylabel('# of non-zero elements in the attack vector');
%legend('Matching Pursuit Algorithm');
grid on
%%%%%%%%%%%%%%%% 118indicetime %%%%%%%%%%%%%%%%%%%%%%%%%%
load('118bus.mat');
x=zeros(1,117);
for i=1:117
    x(i)=i+1;
end
y=timeMP1;
figure
plot(x,y)
xlabel('Indice of target state variables(IEEE118bus)');
ylabel('time(seconds)');
%legend('Matching Pursuit Algorithm');
grid on

%%%%%%%%%%%%%%%%% 118indicenz %%%%%%%%%%%%%%%%%%%%%%%%%
load('118bus.mat');
x=zeros(1,117);
for i=1:117
    x(i)=i+1;
end
y=nonzerosMP1;
figure
plot(x,y)
xlabel('Indice of target state variables(IEEE118bus)');
ylabel('# of non-zero elements in the attack vector');
%legend('Matching Pursuit Algorithm');
grid on
%%%%%%%%%%%%%%%%% baravgtime%%%%%%%%%%%%%%%%%%%%%%%%%
xx=[ 9, 14 30 118 300];
load('Bus300.mat');
avgtime(1)=sum(timeMP1)/299;
avgnz(1)=sum(nonzerosMP1)/299;
load('118bus.mat');
avgtime(2)=sum(timeMP1)/117;
avgnz(2)=sum(nonzerosMP1)/117;
load('30bus.mat');
avgtime(3)=sum(timeMP1)/29;
avgnz(3)=sum(nonzerosMP1)/29;
load('14bus.mat');
avgtime(4)=sum(timeMP1)/13;
avgnz(4)=sum(nonzerosMP1)/13;
load('9bus.mat');
avgtime(5)=sum(timeMP1)/8;
avgnz(5)=sum(nonzerosMP1)/8;
avgtime
avgnz
y=[0.00968   0.02848    0.07758   0.15242   0.2731];
figure
bar(avgtime);
set(gca,'XTickLabel', {'300 bus'; '118 bus'; '30 bus';'14bus';'9bus'} );
ylabel('time(seconds)');
grid on
%%%%%%%%%%%%%%%%% barpercentagenz%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
bar(y);
set(gca,'XTickLabel',  {'300 bus'; '118 bus'; '30 bus';'14bus';'9bus'} );
ylabel('Percentage of non-zero elements in the attack vector');
grid on
