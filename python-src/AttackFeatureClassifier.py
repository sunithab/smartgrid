import numpy as np
import Constants
import ClassifierUtils as cu
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

class AttackFeatureClassifier :
    def __init__(self, trainingData, trainingDataLabels):
        self.featureModels=[]
        self.initializeFeatureModels(trainingData, trainingDataLabels)

    def initializeFeatureModels(self, trainData, trainLabels):
        rows, cols = trainData.shape
        #Train a KNN model for each training data
        for i in range(cols) :
            x = trainData[:, i]
            x = x.reshape(-1, 1)
            x = np.concatenate((x, np.zeros(x.shape)), axis=1)
            self.featureModels.append(self.get_model(x,trainLabels))

    def get_model(self, trainData, trainLabels):
        #model = self.knn_model()
        model = self.svm_model()
        model.fit(trainData, trainLabels)
        return  model

    def knn_model(self):
        return KNeighborsClassifier(n_neighbors=Constants.KNN_NEIGHBORS)

    def svm_model(self):
        return  SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
                              decision_function_shape=None, degree=3, gamma='auto', kernel='rbf',
                              max_iter=-1, probability=True, random_state=None, shrinking=True,
                              tol=0.001, verbose=False)


    def get_prediction_result(self, testDataRow):
        results=[]
        predictionProb=[]
        for index in range(len(self.featureModels)):
            model= self.featureModels[index]
            result = (model.predict([[testDataRow[index], 0.0]]))
            predictionProbability = model.predict_proba([testDataRow[index], 0.0])
            # print "result", result
            if result[0] == Constants.ATTACK_LABEL:
                #results.append((result[0], str(predictionProbability[0][1] * 100)))
                results.append(result[0])
                predictionProb.append(predictionProbability[0][1] * 100)
            elif result[0] == Constants.NO_ATTACK_LABEL:
                #results.append((result[0], str(predictionProbability[0][0] * 100)))
                results.append(result[0])
                predictionProb.append(predictionProbability[0][0] * 100)
            else:
                #results.append((Constants.ERROR_CODE, Constants.ERROR_PRED_PROB))
                results.append(Constants.ERROR_CODE)
                predictionProb.append(Constants.ERROR_PRED_PROB)

        return results, predictionProb

    #This method will detect if any of the meters is affected
    def get_predictions(self, testFileName):
        test_data = cu.get_file_data(testFileName, delimiter=",",dtype=float)
        result=[]
        print "Predicting new data result for smart grid data (normal/attack) : "
        for test_data_rec in test_data :
            (results, predProb) = self.get_prediction_result(test_data_rec)
            if Constants.ATTACK_LABEL in results :
                result.append((Constants.ATTACK_LABEL, np.mean(predProb)))
            else:
                result.append((Constants.NO_ATTACK_LABEL, np.mean(predProb)))
        return result


    def display_result(self, results):
        for result, acc in results:
            if result == Constants.ATTACK_LABEL:
                print "Attack data.. \t PredictionProb: ", acc
            elif result == Constants.NO_ATTACK_LABEL:
                print "Normal Data.. No attack. \t PredictionProb: ", acc
            else:
                print Constants.ERROR_CODE, "No Accuracy"


