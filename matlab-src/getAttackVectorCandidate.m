function a= getAttackVectorCandidate(B_s, y, H, c, numOfAttackedMeters)
    penalty=1; %from paper
    numIterations=10000; %from paper
    C_lambda=1/2; %determines sparness - tocheck and determine optimal value
    lambda =  C_lambda * max(abs(H * c));

    [m,n]=size(H);
    currIteration=0;
    beta = zeros(m,1);
    u = zeros(m,1);
    a = zeros(m,1); 
    I=eye(m);
    
    %TODO 
    while currIteration<numIterations && (nnz(a)==0 || nnz(a)>numOfAttackedMeters) % && and some stopping criteria
        a = inv((B_s' * B_s) + (penalty * I)) * ((B_s' * y) + (penalty * (beta-u))) ;
        a = round(a,3);
        k=lambda/penalty;
        temp = u + a;
        beta = max(temp-k, 0) - max(-temp-k, 0); 
        u = temp - beta;
        currIteration = currIteration+1;
    end  
    %disp(nnz(a))
end
