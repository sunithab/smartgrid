
%IEEE Case14 DC
load('IEEE14DC.mat');
newF=full(TB);
[rows,cols]=size(newF);
H=newF(:,2:cols);
[m,n]=size(H);

%load case14 bus system
mpc = loadcase('case14');
define_constants;

%how many data you want to generate.
length = 2;
%normal data
Z=zeros(m,length);
%normal data with noise
Z1=zeros(m,length);

%generate a series of normal vector z
mpopt=mpoption('out.all',0);
for i=1:length
    results=rundcpf(mpc,mpopt);
    x=results.bus(:,9); %9th column is voltage phase angle
    x=x(2:14,1);
    z=H*x;
    Z(:,i)=z;
    
    %with noise    
    e=normrnd(0,0.1,[m 1]);
    Z1(:,i)=z+e;
end
disp(Z1')
dataPath = fullfile( cd(cd('..')), 'data');
