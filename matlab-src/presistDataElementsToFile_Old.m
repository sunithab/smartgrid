function []= presistDataElementsToFile_Old (caseNum,numOfAttackedMeters,attacktype,Z,Z1,A,A_with_noise,A_attackedMeters,isTestData)
    %This function saves every element in a separate row in the file,
    %as a result, the file will have one column and m X n rows
    dataPath = fullfile( cd(cd('..')), 'data');
    if isTestData 
        dataPath = fullfile( dataPath, 'test');
    end
    dataPath = fullfile( dataPath, attacktype); %eg : lasso , iid or any other
    folderName=sprintf('case_%d',caseNum);
    mkdir(dataPath,folderName);
    dataPath = fullfile( dataPath, folderName);
    mkdir(dataPath,num2str(numOfAttackedMeters));
    dataPath = fullfile( dataPath, num2str(numOfAttackedMeters))

    normalFormatSpec = 'NormalData_IID_case_%d_attack_%d%s.txt';
    attackFormatSpec = '%sAttackData_IID_case_%d_attack_%d%s.txt';

    attackFormatSpecMeters = 'AttackMeters_IID_case_%d_%d.txt';

    %%Generate Training data without noise
    %{     

    trainNormalFormatSpec = 'Train%s_NormalData_IID_case_%d_attack_%d.txt';
    trainNoisyFormatSpec = 'Train%s_NoisyData_IID_case_%d_attack_%d.txt';

    train_data=[A1; Z(A1_attackedMeters==1)];
    train_labels= [A1_attackedMeters; zeros(nnz(A1_attackedMeters),1)];
    dlmwrite(fullfile(dataPath, sprintf(trainNormalFormatSpec, 'Data',caseNum, numOfAttackedMeters)),train_data);
    dlmwrite(fullfile(dataPath, sprintf(trainNormalFormatSpec, 'Label',caseNum, numOfAttackedMeters)),train_labels);
    disp('Generated attack data without noise');

    train_data=[A1_with_noise; Z1(A1_attackedMeters==1)];
    train_labels= [A1_attackedMeters; zeros(nnz(A1_attackedMeters),1)];
    dlmwrite(fullfile(dataPath, sprintf(trainNoisyFormatSpec, 'Data',caseNum, numOfAttackedMeters)),train_data);
    dlmwrite(fullfile(dataPath, sprintf(trainNoisyFormatSpec, 'Label',caseNum, numOfAttackedMeters)),train_labels);
    disp('Generated attack data with noise');
    %} 
    
    newDim=numel(Z);

    dlmwrite(fullfile(dataPath,  sprintf(normalFormatSpec, caseNum, numOfAttackedMeters,'')), reshape(Z, [newDim, 1]));
    disp('Generated normal data without noise');

    dlmwrite(fullfile(dataPath, sprintf(normalFormatSpec, caseNum, numOfAttackedMeters,'_noise')), reshape(Z1, [newDim, 1]));
    disp('Generated normal data with noise');

    dlmwrite(fullfile(dataPath, sprintf(attackFormatSpec, '',caseNum, numOfAttackedMeters,'')), reshape(A, [newDim, 1]));
    disp('Generated attack data without noise');

    dlmwrite(fullfile(dataPath, sprintf(attackFormatSpec, '',caseNum, numOfAttackedMeters,'_noise')), reshape(A_with_noise, [newDim, 1]));
    disp('Generated attack data with noise');

    dlmwrite(fullfile(dataPath, sprintf(attackFormatSpecMeters, caseNum,numOfAttackedMeters)), reshape(A_attackedMeters, [newDim, 1]));
    disp('Generated attacked meters information');
end 
