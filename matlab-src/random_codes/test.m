%IEEE Case9 DC
load('IEEE14DC.mat');
caseNum='case57'
%load case9W bus system
mpc = loadcase(caseNum);
newF=full(TB);
[rows,cols]=size(newF);
H=newF(:,2:cols);
[m,n]=size(H);
fprintf('Dimension of H is %d x %d\n', m, n );
fprintf('Number of meters needed for the attack to be undetected by SVE for IEEE-case-%d : %d\n', caseNum, m-n+1 );


H1= generate_mete_H(caseNum);

[m1,n1]=size(H1);
fprintf('Dimension of H1 is %d x %d\n', m1, n1 );
a= (H==H1)
