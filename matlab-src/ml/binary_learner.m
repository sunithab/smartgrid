clear all
lib_path = fullfile( cd(cd('..')), 'feature_selection_lib');
addpath(lib_path, fullfile(lib_path,'lib'), fullfile(lib_path,'methods'));

data_path = fullfile( cd(cd('../..')), 'data/case_9');
train_data_file_name = fullfile(data_path, 'final_training_case_9_attack_5.txt');
train_label_file_name =  fullfile(data_path, 'final_training_label_case_9_attack_5.txt');

precision = @(confusionMat) (diag(confusionMat)./sum(confusionMat,2));

recall = @(confusionMat) mean(diag(confusionMat)./sum(confusionMat,1)');

f1Scores = @(confusionMat) mean(2*(precision(confusionMat).*recall(confusionMat))./(precision(confusionMat)+recall(confusionMat)))
delimiter_in=','

X = importdata(train_data_file_name,delimiter_in);
Y = importdata(train_label_file_name,delimiter_in);

%model = fitcecoc(X,Y, 'CrossVal' = 5);
%model.predict(X)
%model = multisvm(X,Y,X)

%t = templateSVM('Standardize',1,'KernelFunction','rbf');
%model = fitcecoc(X,Y,'Learners',t,'FitPosterior',1,'Verbose',2);
%P = model.predict(X);
%accuracy = sum(Y == P) / numel(Y);
avg_accuracy=0;
avg_precision=0;
avg_recall=0;
avg_f1=0;

% 10-fold cross-validation on the fisheriris data using linear
% discriminant analysis and the third column as only feature for
% classification

num_folds=10
indices = crossvalind('Kfold',Y ,num_folds);
model_name = 'svm';
feature_trans_name = 'none' %'relieff';


cp = classperf(Y); % initializes the CP object
for i = 1:num_folds
    test = (indices == i); train = ~test;
    %{
    model = fitcecoc(X(train,:),Y(train),'Learners',t,'FitPosterior',1,'Verbose',2);
    P = model.predict(X(test,:));
    %}
    
    %{
    model = multi_one_vs_rest_classification(X(train,:),Y(train))
    P= multisvm_predict(model, X(test,:))
    %}
    train_out=X(train,:); % save original data
    test_out=X(test,:);
    train_label = Y(train);
    test_label=Y(test);
    

    %{
    [coefs, dims] = pca_params(train_out);
    train_out = pca_transform(coefs, dims, train_out);
    test_out = pca_transform(coefs, dims, test_out);
    %}
    
    
    
    %{
    maxdev = chi2inv(.95,1);     
    opt = statset('display','iter','TolFun',maxdev,'TolTypeFun','abs');
    inmodel = sequentialfs(@critfun,train_out,train_label, 'cv','none','nullmodel',true, 'options',opt,'direction','backward');
    train_out = train_out(:,inmodel);
    test_out = test_out(:, inmodel);
    %}
    %{
    c = cvpartition(train_label,'k',2);
    opts = statset('display','iter');
    fun = @(XT,yT,Xt,yt) (sum(~strcmp(yt,classify(Xt,XT,yT))));

    [fs,history] = sequentialfs(fun,train_out,train_label,'cv',c,'options',opts, 'direction', 'backward');
    train_out = train_out(:,fs);
    test_out = test_out(:, fs);
    %}
    %t = templateTree('MinLeafSize',5);
    %rusTree = fitensemble(train_out,train_label,'RUSBoost',1000,t, 'LearnRate',0.1,'nprint',100);
    %P=predict(rusTree,test_out)
    %rng default % For reproducibility
    %T = 500;
    %adaStump = fitensemble(X,Y,'LPBoost',T,'Tree');
    %adaStump = fitensemble(X,Y,'TotalBoost',T,'Tree');
    %adaStump = fitensemble(train_out,train_label,'AdaBoostM1',T,'Tree');
    %P=predict(adaStump,test_out)

   
    [models, fea_trans] = classifier(train_out, train_label, model_name, feature_trans_name);
    P= predictor( test_out, models, model_name, fea_trans, feature_trans_name);
    

    % updates the CP object with the current classification results
    classperf(cp,P,test)  
    conf_martix = confusionmat(test_label, P);
    avg_accuracy = avg_accuracy+(sum(test_label == P) / numel(test_label));
    avg_precision= avg_precision+ precision(conf_martix);
    avg_recall= avg_recall + recall(conf_martix);
    avg_f1= avg_f1 + f1Scores(conf_martix);
end
cp.CorrectRate % queries for the correct classification rate
avg_accuracy = avg_accuracy/num_folds
avg_precision=avg_precision/num_folds
avg_recall=avg_recall/num_folds
avg_f1=avg_f1/num_folds

%accuracy = sum(Y == P) / numel(Y)


