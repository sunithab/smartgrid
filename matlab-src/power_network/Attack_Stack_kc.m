function [attackvector]=Attack_Stack_kc(H,k,cc)
flag=0;
[rows,cols]=size(H);
% for i=1:cols
%       H(:,i)=H(:,i)+b;
% end
maxvector=H(:,1);
max=zerocounter(H(:,1));
for i=2:cols
    if(zerocounter(H(:,i))>=max)
       maxvector=H(:,i);
       max=zerocounter(H(:,i));
    end
end
stack=H;
while (~isempty(stack))
     h=stack(:,1);
     %%%%%%%%%%%%%%%%%%%
     if(zerocounter(h)>=rows-k)
           flag=1;
           break;
     end
     %%%%%%%%%%%%%%%%%%%
     stack(:,1)=[];
     [m, n]=size(stack);
     length_n=n;
     for i=1:length_n
         t=stack(:,i);
         if(norm(h-t)<1e-6)
            continue;
         end
        [newh,d]=diffi(h',t'); 
        newh=newh'; 
         if(d<=0) 
            if(max<zerocounter(newh))
                  max=zerocounter(newh);
                  maxvector=newh;
                  stack=horzcat(stack,newh);
            end
         end
     end
end
if(flag==1)
   fprintf('\nStack Kc Bingo! The %d  attack vector is:\n',rows-k);
   attackvector=h;
else
  fprintf('\nKc can not find a %d attack vector!, the best attack vector has %d zero elements\n',rows-k,max);
  attackvector=maxvector;
end
c=inv(H'*H)*H'*attackvector;
%%
% [m,n]=size(H);
% temp=H;
% b=zeros(m,1);
% offset=0;
% for i=1:length(cc)
%    if(cc(i)~=0)
%         b=b+cc(i)*temp(:,i-offset);
%         temp(:,i-offset)=[];
%         offset=offset+1;
%    end
% end
% H1=temp;
% if(rank(H1,10e-5)~=rank(horzcat(H1,b),10e-5))
%      fprintf('can not find solution!');
%      DeltaC=zeros(length(c),1);
% else
%     [mI,mI]=size(pinv(H1)*H1);
%     I=eye(mI);
%     z=10*ones(mI,1);
%     DeltaC=pinv(H1,1e-5)*b+(I-pinv(H1,1e-5)*H1)*z;
% end
% c=c+DeltaC;
%%
for i=1:length(cc)
   if(cc(i)~=0)
       if(c(i)>=1e-5)
               c=c*(cc(i)/c(i));
               attackvector=H*c;
       else
               attackvector=H*c+cc(i)*H(:,i);
       end
   end
end


