import numpy as np


def get_file_data(fileName, delimiter=",", dtype=float):
    return np.genfromtxt(fileName, delimiter=delimiter, dtype=dtype)


def get_classifier_metrics(tp, tn, fp, fn):
    recall = tp * 1. / (tp + fn)
    precision = tp * 1. / (tp + fp)
    accuracy = (tp + tn) * 1. / (tp + tn + fp + fn)
    specificity_tnr = tn * 1. / (tn + fp)
    f1_score = (2. * tp) / (2. * tp + fp + fn)

    print "Recall: ", recall
    print "Precision: ", precision
    print "Accuracy : ", accuracy
    print "Specificity: ", specificity_tnr
    print "F1 Score: ", f1_score