function [d,newh]=mydiff(a,b)
flag=0;
d=0;
incr=0;
decr=0;
length=size(a);
for i=1:length
     if((abs(a(i))<1e-10)&&(abs(b(i))<1e-10))
        decr=decr+1;
     end
end
for i=1:length
     if((abs(a(i))>1e-10)&&(abs(b(i))>1e-10))
        newh=a-(a(i)/b(i))*b;
        flag=1;
        break;
     end
end
zerocounter1=0;
zerocounter2=0;
if(flag==1)
  for i=1:length
     if(abs(newh(i))<1e-10)
        zerocounter1=zerocounter1+1;
     end
     if(abs(a(i))<1e-10)
        zerocounter2=zerocounter2+1;
     end
  end
  d=zerocounter1-zerocounter2;
else
  d=incr-decr;
  newh=a;
end

 